﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class AudioController : MonoBehaviour {

    public AudioSource Hit, Swing, Death, Noticed;
    private List<AudioSource> sources;

    void Start()
    {
        Hit = getDefaultSource(Hit, "MeleeGroanSource");
        Swing = getDefaultSource(Swing, "MeleeSwingSource0");
        Death = getDefaultSource(Death, "DeathSource0");
        Noticed = getDefaultSource(Noticed, "Noticed0");
        sources = new List<AudioSource>
        {
            Hit, Swing, Death, Noticed
        };
    }
    
    public void StopAll()
    {
        foreach (var source in sources) {
            source.Stop();
        }
    }

    private static AudioSource getDefaultSource(AudioSource source, string tag)
    {
        if (source)
        {
            return source; 
        }
        return getSource(tag);
    }

    private static AudioSource getSource(string tag)
    {
        return GameObject.FindGameObjectWithTag(tag).GetComponent<AudioSource>();
    }
}