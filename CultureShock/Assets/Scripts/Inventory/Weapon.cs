﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Weapon : Item {

	public int attack;
    public float length;
	public bool ranged;
	
    public Weapon()
    {
        this.title = "";
        this.price = 0;
        this.attack = 0;
        this.length = 2.0F;
        this.count = 1;
        this.pic = null;
        this.ranged = false;
		this.description = "";
    }

	public Weapon(string title, int price, bool unique, int attack, float length, bool ranged, string des)
	{
		this.title = title;
		this.price = price;
		this.attack = attack;
        this.length = length;
		this.count = 1;
		this.pic = Resources.Load("Sprites/" + this.title) as Texture;
		this.sprite = Resources.Load<Sprite>("Sprites/" + this.title);
		this.ranged = ranged;
		this.unique = unique;
		this.description = des;
	}
    
	public void ReloadSprite()
	{
		this.sprite = Resources.Load<Sprite>("Sprites/" + this.title);
	}

	public override void Use(GameObject user, List<Item>  list)
    {
		//user.transform.FindChild("Equipment").GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite> ("Sprites/" + this.title);
        user.GetComponentInChildren<EquippedWeapon>().SetWeapon(this);
    }

    public override string GetToolTip(GameObject user)
    {
        //var equippedWeapon = user.GetComponentInChildren<EquippedWeapon>();
        // TODO: basic attack
        return title + "\nAttack: " + this.attack + " -> " + user.GetComponent<Strength>().GetDamageWithWeapon(this);
    }
    
    public override void DrawLabels(Rect area, GameObject user)
    {
        base.DrawLabels(area, user);
        if (title == user.GetComponentInChildren<EquippedWeapon>().title)
        {
            DrawEquippedLabel(area);
        }
    }
}