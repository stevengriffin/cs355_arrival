﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Food : Item {

	public int recover;
	public Food(){}
	public Food(string title, int price, bool unique, int recover, string des)
	{
		this.title = title;
		this.price = price;
		this.recover = recover;
		this.pic = Resources.Load("Sprites/" + this.title) as Texture;
		this.unique = unique;
		this.description = des;
	}

    public override void Use(GameObject user, List<Item> list)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Health>().Add(recover);
        DecrementInvCount(list);
    }

    public override string GetToolTip(GameObject user)
    {
        return title + "\nHealth + " + recover;
    }
}
