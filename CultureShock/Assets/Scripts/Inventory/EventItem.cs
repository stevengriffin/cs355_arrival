﻿using UnityEngine;
using System.Collections;

public class EventItem : Item {
	public EventItem(){}
	public EventItem(string title, int price, bool unique, string des)
	{
		this.title = title;
		this.price = price;
		this.pic = Resources.Load("Sprites/" + this.title) as Texture;
		this.unique = unique;
		this.description = des;
	}
	public override string GetToolTip(GameObject user)
	{

		return title;
	}
}
