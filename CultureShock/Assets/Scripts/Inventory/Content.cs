﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Serialization;

[SerializeAll]
public class Content : MonoBehaviour {

	public TextAsset itemFile;
	public static Dictionary<string,Item> fullCategory = new Dictionary<string,Item>();
	
	void Start () {
		// Read from file
		try
		{
			string text = itemFile.text;
			string[] lines = text.Split('\n');
			for(int i = 0; i < lines.Length; i++)
			{
				string line = lines[i];
				string description = getString(line, "$DESS$", "$DESE$");
				string name = getString(line, "$NAMES$", "$NAMEE$");
				string[] parts = line.Split(default(string[]), System.StringSplitOptions.RemoveEmptyEntries);
				
				bool unique;
				if (parts[4] == "t") unique = true;
				else unique = false;
				
				switch (parts[0])
				{
				case "A":
					fullCategory.Add(name, new Armor(name, 
						Convert.ToInt32(parts[3]), unique, Convert.ToInt32(parts[5]), description));
					break;
				case "F":
					fullCategory.Add(name, new Food(name, 
						Convert.ToInt32(parts[3]), unique, Convert.ToInt32(parts[5]), description));			
					break;
				case "W":
					bool ranged;
					if (parts[4].Equals("t")) ranged = true;
					else ranged = false;
					fullCategory.Add(name, new Weapon(name, 
						Convert.ToInt32(parts[3]), unique, Convert.ToInt32(parts[5]), Convert.ToInt32(parts[6]), ranged, description));
					break;
				case "E":
					fullCategory.Add(name, new EventItem(name,
						Convert.ToInt32(parts[3]), unique, description));
					break;
				}
			}
		}
		catch (Exception e)
		{
			Debug.Log(e);
		}
		if (LevelSerializer.IsDeserializing) 
		{
			return;
		}
	}
	
	private string getString(string origin, string startMarker, string endMarker)
	{
		int start = origin.IndexOf(startMarker);
		int end = origin.IndexOf(endMarker);
		int offset = startMarker.Length;
		return  origin.Substring(start + offset, end - offset - start);
	}
	
	public Item getItem(string name)
	{
		if (fullCategory.ContainsKey(name)) return fullCategory[name];
		return null;
	}
}
