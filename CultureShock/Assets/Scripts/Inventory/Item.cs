﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Serialization;

[SerializeAll]
public abstract class Item {

	// 1 letter + 3 digit number
	// letter: F for food; W for weapon; A for Armor; E for event items
	// 3 digit is their number in category
	public string code = "";
	// name visible to player
	public string title = "";
	public int count = 1;
	public int invCount = 0;
	public int shopCount = 0;
	public int price = 0;
	public string description = "";
	public bool unique = false;
	public Texture pic;
	public Sprite sprite;

	public void DecrementInvCount(List<Item> list)
    {
        invCount--;
        if (invCount == 0)
        {
            list.Remove(this);
        }
    }

	// Items do nothing by default
    public virtual void Use(GameObject user, List<Item> list)
    {
    }

    // Items have no tooltip by default
    public virtual string GetToolTip(GameObject user)
    {
        return "";
    }

    public virtual void DrawLabels(Rect area, GameObject user)
    {
    	if (!unique)
    	{
			GUI.Label(new Rect(area.xMin-6f, area.yMax - 28f, 20f, 20f), invCount.ToString());
    	}
    }

    public virtual void DrawEquippedLabel(Rect area)
    {
        GUI.Label(new Rect(area.xMax-22f, area.yMax - 28f, 20f, 20f), "E");
    }
}