﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Serialization;

[SerializeAll]
public class Inventory : MonoBehaviour {
	public List<Item> foodList = new List<Item>();
	public List<Item> weaponList = new List<Item>();
	public List<Item> armorList = new List<Item>();
	public List<Item> eventList = new List<Item>();
	public int money;
	public GUISkin skin;
	public bool open;

	private Vector2 scrollPosition;
	private AudioSource button, pickup, equipArmor, equipWeapon, eatFood;
	private float timer = 0;
	private float speed = 0.5f;
	private const float respondTime = 0.8f;
	//private Item label;
    private GameObject player;

    // Make get method less of an eyesore. Should probably fix problem at its source in Content, though.
	private Dictionary<State, List<Item>> contentListDict;
	
	enum State
	{
		Food,
		EventItem,
		Weapon,
		Armor
	};
	private State state;
	
	void Start () 
	{
		this.state = State.Food;
		//this.label = new Armor(); //Placeholder
		this.open = false;
		this.scrollPosition = Vector2.zero;
		pickup = gameObject.AddComponent<AudioSource>();
		pickup.clip = Resources.Load("Audio/pickup") as AudioClip;
		equipArmor = gameObject.AddComponent<AudioSource>();
		equipArmor.clip = Resources.Load("Audio/equip_armor") as AudioClip;
		equipWeapon = gameObject.AddComponent<AudioSource>();
		equipWeapon.clip = Resources.Load("Audio/equip_weapon") as AudioClip;
		eatFood = gameObject.AddComponent<AudioSource>();
		eatFood.clip = Resources.Load("Audio/eat_food") as AudioClip;
		button = gameObject.AddComponent<AudioSource>();
		button.clip = Resources.Load("Audio/click_button") as AudioClip;
        player = GameObject.FindGameObjectWithTag("Player");
		contentListDict = new Dictionary<State, List<Item>>
		{
			{ State.Armor, armorList },
			{ State.EventItem, eventList },
			{ State.Weapon, weaponList },
			{ State.Food, foodList },
		};
		if (LevelSerializer.IsDeserializing) 
		{
			return;
		}
		get((Item)Content.fullCategory["Meat"]);
		//Item weap = (Item)Content.fullCategory ["pipe"];
		//get(weap);
		//weap.Use(player, null);
		get((Item)Content.fullCategory["Cloth Shirt"]);
		money = 500;
	}
	
	void Update () 
	{

		gameObject.transform.position = player.transform.position;
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = GameObject.FindGameObjectWithTag ("MainCamera").camera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit) && Content.fullCategory.ContainsKey(hit.transform.gameObject.name)) {
				if (Vector3.Distance(hit.transform.position, player.transform.position) < 8f)
				{
					GUIStyle sty = new GUIStyle();
					sty.fontSize = 12;
					sty.normal.textColor = Color.white;
					sty.wordWrap = true;
					sty.alignment = TextAnchor.LowerCenter;
					
					TextFader tf = gameObject.AddComponent<TextFader> ();
					tf.t = hit.transform;
					tf.duration = 2.0f;
					tf.text = "Picked up " + hit.transform.gameObject.name;
					tf.style = sty;
					tf.outColor = Color.black;
					tf.inColor = Color.gray;

					get((Item)Content.fullCategory[hit.transform.gameObject.name]);
					Destroy(hit.transform.gameObject);
					pickup.Play();	
				}	
			}
		}
	}
	
    private static Rect calculateTargetArea(int count)
    {
        return new Rect(1.5f + 35f*nextX(count), 25f + 1.5f + 35f*nextY(count), 32f, 32f);
    }

    private static int nextY(int count) { return count / 4; }

    private static int nextX(int count) { return count % 4; }

    // give info when place mouse on icon
    private void showToolTipOnMouseOver(Rect targetArea, string info, string description)
    {
        if (targetArea.Contains(Event.current.mousePosition))
        {
            this.timer += this.speed * Time.deltaTime;
            if (this.timer >= respondTime)
            {
            	info += "\n" + description;
                GUI.Box(new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y - 20, Math.Max(info.Length, description.Length) * 4, 60), info);
            }
        }
    }

	private void handleInventoryState(List<Item> list)
    {
        // This loop might remove items so we go backwards.
        for (int count = list.Count - 1; count >= 0; count--)
        {
            Item item = list[count];
            var targetArea = calculateTargetArea(count);
            if (GUI.Button(targetArea, item.pic))
            {
                item.Use(player, list);
                this.playUse(this.state);
            }
			item.DrawLabels(targetArea, player);
            // redraw to keep on top
            showToolTipOnMouseOver(targetArea, item.GetToolTip(player), item.description);
            
        }
    }

	private void CloseOtherWindow()
	{
		var attributeObject = GameObject.FindGameObjectWithTag("Attribute").GetComponent<Attribute>();
		attributeObject.open = false;
		var missionObject = GameObject.FindGameObjectWithTag("Mission").GetComponent<MissionInterface>();
		missionObject.isOpen = false;
	}

	void OnGUI()
	{
		GUI.skin = skin;
		Ray ray = GameObject.FindGameObjectWithTag ("MainCamera").camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit) && Content.fullCategory.ContainsKey(hit.transform.gameObject.name)) {
			if (Vector3.Distance(hit.transform.position, player.transform.position) < 5f)
			{
				string info = "Click to pickup...";
				GUI.Box(new Rect(Input.mousePosition.x, Screen.height-Input.mousePosition.y, info.Length * 5, 25), info);
			}	
		}
		// Open or close
		if (GUI.Button(new Rect(10, 10, 100, 25), "Inventory"))
		{
			this.open = !this.open;
			if (open) CloseOtherWindow();
			button.Play();
		}
		
		// Set up inventory interface
		if (this.open)
		{
			GUI.Box(new Rect(10, 35, 280, 200), "");
			this.scrollPosition = GUI.BeginScrollView (new Rect (10,35,280,200), this.scrollPosition, new Rect (0, 0, 200, 200));
			if (GUI.Button(new Rect(5,0,60,25),"Food"))
			{
                state = State.Food;
			}
			if (GUI.Button(new Rect(75,0,60,25),"Event"))
			{
				state = State.EventItem;
			}
			if (GUI.Button(new Rect(145,0,60,25),"Weapon"))
			{
				state = State.Weapon;
			}
			if (GUI.Button(new Rect(215,0,60,25),"Armor"))
			{
				state = State.Armor;
			}
            handleInventoryState(contentListDict[state]);
            GUI.EndScrollView();
		}
	}

	private void getGeneric(Item newItem, List<Item> list)
    {
        foreach (Item content in list)
        {
            if (content.title == newItem.title)
            {
                content.invCount += newItem.count;
                return;
            }
        }
        newItem.invCount += newItem.count;
        list.Add(newItem);
    }

	public void get(Item newItem)
	{
		if (newItem.GetType () == typeof(Food)) 
		{
			getGeneric (newItem, contentListDict [State.Food]);
		}
		else if (newItem.GetType () == typeof(EventItem)) 
		{
			getGeneric (newItem, contentListDict [State.EventItem]);
		}
		else if (newItem.GetType () == typeof(Weapon)) 
		{
			getGeneric (newItem, contentListDict [State.Weapon]);
		}
		else if (newItem.GetType () == typeof(Armor)) 
		{
			getGeneric (newItem, contentListDict [State.Armor]);
		}
	}
	
	private void playUse(State state)
	{
		if (state == State.Food) eatFood.Play();
		else if (state == State.Armor) equipArmor.Play();
		else if (state == State.Weapon) equipWeapon.Play();
	}
}