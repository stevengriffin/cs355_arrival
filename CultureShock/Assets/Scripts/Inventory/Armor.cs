﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Armor : Item {
	public int defense;
    // Default constructor if you have no armor
	public Armor()
    {
        this.title = "";
        this.price = 0;
        this.defense = 0;
        this.count = 1;
        this.pic = null;
        this.description = "";
    }

	public Armor(string title, int price, bool unique, int defense, string des)
	{
		this.title = title;
		this.price = price;
		this.defense = defense;
		this.count = 1;
		this.pic = Resources.Load("Sprites/" + this.title) as Texture;
		this.sprite = Resources.Load<Sprite>("Sprites/" + this.title);
		this.unique = unique;
		this.description = des;
	}

    public override void Use(GameObject user, List<Item> list)
    {
        user.GetComponentInChildren<EquippedArmor>().SetArmor(this);   
    }

    public override string GetToolTip(GameObject user)
    {
        var defenseComp = user.GetComponent<Defense>();
		return title + "\nDefense: " + this.defense + " -> " + (int)(defenseComp.baseDefense + defense);
    }
    
    public override void DrawLabels(Rect area, GameObject user)
    {
        base.DrawLabels(area, user);
        if (title == user.GetComponentInChildren<EquippedArmor>().title)
        {
            DrawEquippedLabel(area);
        }
    }
}
