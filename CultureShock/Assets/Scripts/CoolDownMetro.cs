﻿using UnityEngine;
using System.Collections;

public class CoolDownMetro {

    public double duration;
    private bool hasFired = false;
    private double firedTime = 0.0;

	public CoolDownMetro()
	{
	}

    public CoolDownMetro(double duration)
    {
        this.duration = duration;
    }
	
	// Update is called once per frame
	public bool Update () {
        if (hasFired && Time.time - firedTime >= duration)
        {
            hasFired = false;
        }
        return !hasFired;
	}

    public bool Fire()
    {
        if (hasFired)
        {
            return false;
        }
        hasFired = true;
        firedTime = Time.time;
        return true;
    }

    public float GetPercent()
    {
        if (hasFired)
        { 
            return (float) ((Time.time - firedTime) / duration);
        }
        return 1.0F;
    }

    public bool HasFired()
    {
        return hasFired;
    }
}