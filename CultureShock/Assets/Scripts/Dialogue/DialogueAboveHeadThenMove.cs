﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
[SerializeAll]
public class DialogueAboveHeadThenMove : DialogueAboveHead 
{
	public Vector3 waypoint;
	public int speed = 1;
	protected void OnGUI () 
	{
		base.OnGUI();
		if(ds == DialogueState.FINAL)
		{
			WaypointPath wp = gameObject.AddComponent<WaypointPath>();
			wp.waypoints = new Vector3[1];
			wp.waypoints[0] = waypoint;
			wp.speed = speed;
			wp.loop = false;

			ds = DialogueState.DONE;
		}
	}
	
	public void missionShow()
	{
		this.OnGUI();
	}
	

}
