﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
[SerializeAll]
public class DialogueLineAbove
{
	public Transform character;
	public string dialogueLine;
	public int lineOffset = 50;
	public Font font;
	public bool richText = false;
	public DialogueLineAbove()
	{
	}
	public DialogueLineAbove(Transform character, string dialogueLine)
	{
		this.character = character;
		this.dialogueLine = dialogueLine;
	}
	public DialogueLineAbove(Transform character, string dialogueLine, Font font)
	{
		this.character = character;
		this.dialogueLine = dialogueLine;
		this.font = font;
	}
}

[SerializeAll]
public class DialogueAboveHead : MonoBehaviour 
{
	public enum DialogueMode
	{
		AUTO,
		AUTO_ONCE,
		AUTO_THEN_MANUAL,
		MANUAL,
		MANUAL_ONCE
	};
	public DialogueMode dialogueMode = DialogueMode.MANUAL;
	public float lineDuration = 2.0f;
	public DialogueLineAbove[] dialogue;
	public double textBoxWidthPercentage = .2;
	public float speechBubbleOffsetX;
	public float speechBubbleOffsetY = 9;
	public Font defaultFont;
	public Camera cam = Camera.main;

	public enum DialogueState
	{
			WAITING,
			IN_RANGE,
			TALKING,
			FINAL,
			DONE
	};

	protected float startTime;
	protected int curLine = 0;
	protected bool outOfTrigger = true;
	protected Collider tcollider;
	protected GUIStyle style;
	protected GameObject speechBubble;
	protected DialogueState ds = DialogueState.WAITING;

	public DialogueState GetDialogueState()
	{
		return ds;
	}

	public void Reset()
	{
		curLine = 0;
		outOfTrigger = true;
		ds = DialogueState.WAITING;
	}

	void Start()
	{
		outOfTrigger = true;
	}

	public void TriggerDialogue()
	{
		startTime = Time.time;
		ds = DialogueState.TALKING;
	}

	void OnTriggerEnter ( Collider collider ) 
	{
		if (enabled && collider.CompareTag("Player") && ds == DialogueState.WAITING && outOfTrigger) 
		{
			tcollider = collider;
			if(dialogueMode == DialogueMode.AUTO || dialogueMode == DialogueMode.AUTO_THEN_MANUAL || dialogueMode == DialogueMode.AUTO_ONCE)
			{
				tcollider.gameObject.GetComponent<AnimationTriggers>().Enabled(false);
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = false;
				if(gameObject.GetComponent<WaypointPath>())
				{
					gameObject.GetComponent<WaypointPath>().SetIsActive(false);
				}
				startTime = Time.time;
				ds = DialogueState.TALKING;
				if(dialogueMode == DialogueMode.AUTO_THEN_MANUAL)
				{
					dialogueMode = DialogueMode.MANUAL;
				}
			}
			else
			{
				ds = DialogueState.IN_RANGE;

				speechBubble = (GameObject)Instantiate(Resources.Load("Prefabs/SpeechBubble"));
				if(speechBubble)
				{
					//Debug.Log("Could find SpeechBubble resource.");
					speechBubble.transform.parent = this.gameObject.transform;
					speechBubble.transform.localScale = new Vector3(0,0,0);
					speechBubble.transform.localPosition = new Vector3(speechBubbleOffsetX,speechBubbleOffsetY,0);
					speechBubble.animation.CrossFadeQueued("SpeechBubble_Pulse");
				}
				else
				{
					Debug.Log("Could not find SpeechBubble resource.");
				}
			}
			
			outOfTrigger = false;
		}
	}

	void OnTriggerExit( Collider collider){
		if (collider.CompareTag ("Player") && ds != DialogueState.FINAL && ds != DialogueState.DONE) {
			outOfTrigger = true;
			if(ds == DialogueState.IN_RANGE)
			{
				speechBubble.animation.Play("SpeechBubble_Close");
				DestroyObject(speechBubble,1);
			}
			ds = DialogueState.WAITING;
		}
	}

	protected void OnGUI () 
	{
		if (ds == DialogueState.IN_RANGE) 
		{
			if(Event.current.type == EventType.MouseDown && Event.current.button == 0)
			{
				speechBubble.animation.Play("SpeechBubble_Close");
				DestroyObject(speechBubble,1);
				tcollider.gameObject.GetComponent<AnimationTriggers>().Enabled(false);
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = false;

				if(gameObject.GetComponent<WaypointPath>())
				{
					gameObject.GetComponent<WaypointPath>().SetIsActive(false);
				}
				//style = new GUIStyle ();
				//style.fontSize = 20;
				startTime = Time.time;
				ds = DialogueState.TALKING;
			}
		}
		else if (ds == DialogueState.TALKING) 
		{
			Vector3 pos = cam.WorldToViewportPoint(dialogue[curLine].character.position);
			int textx = (int)(pos.x*Screen.width) - (int)(textBoxWidthPercentage*Screen.width/2);
			int texty = (int)((1-pos.y)*Screen.height) - 200;

			style = new GUIStyle();
			style.font = defaultFont;
			style.fontSize = 25;
			//style.fontStyle = FontStyle.Bold;
			style.normal.textColor = Color.white;
			style.wordWrap = true;
			style.alignment = TextAnchor.LowerCenter;
			//GUI.Box(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width),Screen.height-120,(int)(textBoxWidthPercentage*Screen.width),110),"");
			//GUI.Box(new Rect(textx,texty,(int)(textBoxWidthPercentage*Screen.width),110),"");
			//GUI.Label(new Rect((int)(((1+textBoxWidthPercentage)/2)*Screen.width) - 140,Screen.height-40,(int)(textBoxWidthPercentage*Screen.width)-15,100), "Click to continue...", style);
			//GUI.Label(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width) + 15,Screen.height-100,(int)(textBoxWidthPercentage*Screen.width)-15,100),dialogue[curLine].charName + ": " ,style);
			//GUI.Label(new Rect(textx + 15, texty,(int)(textBoxWidthPercentage*Screen.width)-15,100),dialogue[curLine].charName + ": " ,style);
			style.font = dialogue[curLine].font;
			style.richText = dialogue[curLine].richText;
			//GUI.Label(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width) + 15 + dialogue[curLine].lineOffset,Screen.height-100,(int)(textBoxWidthPercentage*Screen.width)-20-dialogue[curLine].lineOffset,100), dialogue[curLine].dialogueLine, style);
			Color inColor = dialogue[curLine].character.tag == "Player" ? Color.yellow : Color.white;
			DrawOutline(new Rect(textx, texty,(int)(textBoxWidthPercentage*Screen.width),100), dialogue[curLine].dialogueLine, style, Color.black, inColor);
			if ((Time.time > startTime + lineDuration) || (Event.current.type == EventType.MouseDown && Event.current.button == 0)) 
			{ 
				curLine++;
				startTime = Time.time;
				if (curLine >= dialogue.Length) 
				{
					curLine = 0;
					ds = DialogueState.WAITING;
					if(tcollider)
					{
						tcollider.gameObject.GetComponent<AnimationTriggers>().Enabled(true);
						tcollider.gameObject.GetComponent<CharacterMotor>().canControl = true;
					}
					if(gameObject.GetComponent<WaypointPath>())
					{
						gameObject.GetComponent<WaypointPath>().SetIsActive(true);
					}
					speechBubble = null;
					if(dialogueMode == DialogueMode.AUTO_ONCE || dialogueMode == DialogueMode.MANUAL_ONCE)
					{
						ds = DialogueState.FINAL;

					}
				}
			}
		}
	}

	public static void DrawOutline(Camera cam, Transform t, string text, GUIStyle style, Color outColor, Color inColor)
	{
		float textBoxWidthPercentage = 0.2f;
		Vector3 pos = cam.WorldToViewportPoint(t.position);
		int textx = (int)(pos.x*Screen.width) - (int)(textBoxWidthPercentage*Screen.width/2);
		int texty = (int)((1-pos.y)*Screen.height) - 200;
		DrawOutline (new Rect (textx, texty, (int)(textBoxWidthPercentage * Screen.width), 100), text, style, outColor, inColor); 
	}
	public static void DrawOutline(Rect position, string text, GUIStyle style, Color outColor, Color inColor)
	{
		float oldx = position.x;
		float oldy = position.y;
		int offsetSize = 2;
		GUIStyle backupStyle = style;
		style.normal.textColor = outColor;

		for(int x = -offsetSize; x <= offsetSize; x++)
		{
			for(int y = -offsetSize; y <= offsetSize; y++)
			{
				if(!(x == 0 && y == 0))
				{
					position.x = oldx + x;
					position.y = oldy + y;
					GUI.Label(position, text, style);
				}
			}
		}
	
		position.x = oldx;
		position.y = oldy;
		style.normal.textColor = inColor;
		GUI.Label(position, text, style);
		style = backupStyle;
	}


}
