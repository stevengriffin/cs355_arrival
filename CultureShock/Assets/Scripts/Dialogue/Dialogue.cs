﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class DialogueLine
{
	public string charName;
	public string dialogueLine;
	public int lineOffset = 50;
	public Font font;
	public DialogueLine()
	{
	}
	public DialogueLine(string charName, string dialogueLine, int lineOffset, Font font)
	{
		this.charName = charName;
		this.dialogueLine = dialogueLine;
		this.lineOffset = lineOffset;
		this.font = font;
	}
}

public class Dialogue : MonoBehaviour 
{
	public enum DialogueMode
	{
		AUTO,
		AUTO_ONCE,
		AUTO_THEN_MANUAL,
		MANUAL,
		MANUAL_ONCE
	};
	public DialogueMode dialogueMode = DialogueMode.MANUAL;
	public DialogueLine[] dialogue;
	public double textBoxWidthPercentage = .4;
	public float speechBubbleOffsetX;
	public float speechBubbleOffsetY = 9;
	public Font defaultFont;

	enum DialogueState
	{
			WAITING,
			IN_RANGE,
			TALKING
	};

	private int curLine = 0;
	private bool outOfTrigger = true;
	private Collider tcollider;
	private GUIStyle style;
	private GameObject speechBubble;
	private DialogueState ds = DialogueState.WAITING;

	void OnTriggerEnter ( Collider collider ) 
	{
		if (enabled && collider.CompareTag("Player") && ds == DialogueState.WAITING && outOfTrigger) 
		{
			tcollider = collider;
			if(dialogueMode == DialogueMode.AUTO || dialogueMode == DialogueMode.AUTO_THEN_MANUAL || dialogueMode == DialogueMode.AUTO_ONCE)
			{
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = false;
				if(gameObject.GetComponent<WaypointPath>())
				{
					gameObject.GetComponent<CharacterMotor>().inputMoveDirection = Vector3.zero;
					gameObject.GetComponent<WaypointPath>().enabled = false;
				}
				ds = DialogueState.TALKING;
				if(dialogueMode == DialogueMode.AUTO_THEN_MANUAL)
				{
					dialogueMode = DialogueMode.MANUAL;
				}
			}
			else
			{
				ds = DialogueState.IN_RANGE;

				speechBubble = (GameObject)Instantiate(Resources.Load("Prefabs/SpeechBubble"));
				if(speechBubble)
				{
					//Debug.Log("Could find SpeechBubble resource.");
					speechBubble.transform.parent = this.gameObject.transform;
					speechBubble.transform.localScale = new Vector3(0,0,0);
					speechBubble.transform.localPosition = new Vector3(speechBubbleOffsetX,speechBubbleOffsetY,0);
					speechBubble.animation.CrossFadeQueued("SpeechBubble_Pulse");
				}
				else
				{
					Debug.Log("Could not find SpeechBubble resource.");
				}
			}
			
			outOfTrigger = false;
		}
	}

	void OnTriggerExit( Collider collider){
		if (collider.CompareTag ("Player")) {
			outOfTrigger = true;
			if(ds == DialogueState.IN_RANGE)
			{
				speechBubble.animation.Play("SpeechBubble_Close");
				DestroyObject(speechBubble,1);
			}
			ds = DialogueState.WAITING;
		}
	}

	void OnGUI () 
	{
		if (ds == DialogueState.IN_RANGE) 
		{
			if(Event.current.type == EventType.MouseDown && Event.current.button == 0)
			{
				speechBubble.animation.Play("SpeechBubble_Close");
				DestroyObject(speechBubble,1);
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = false;

				if(gameObject.GetComponent<WaypointPath>())
				{
					gameObject.GetComponent<WaypointPath>().enabled = false;
				}
				//style = new GUIStyle ();
				//style.fontSize = 20;
				
				ds = DialogueState.TALKING;
			}
		}
		else if (ds == DialogueState.TALKING) 
		{
			style = new GUIStyle();
			style.font = defaultFont;
			style.normal.textColor = Color.white;
			style.wordWrap = true;
			GUI.Box(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width),Screen.height-120,(int)(textBoxWidthPercentage*Screen.width),110),"");
			GUI.Label(new Rect((int)(((1+textBoxWidthPercentage)/2)*Screen.width) - 140,Screen.height-40,(int)(textBoxWidthPercentage*Screen.width)-15,100), "Click to continue...", style);
			GUI.Label(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width) + 15,Screen.height-100,(int)(textBoxWidthPercentage*Screen.width)-15,100),dialogue[curLine].charName + ": " ,style);
			style.font = dialogue[curLine].font;
			GUI.Label(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width) + 15 + dialogue[curLine].lineOffset,Screen.height-100,(int)(textBoxWidthPercentage*Screen.width)-20-dialogue[curLine].lineOffset,100), dialogue[curLine].dialogueLine, style);
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0) 
			{ 
				curLine++;
				if (curLine >= dialogue.Length) 
				{
					curLine = 0;
					ds = DialogueState.WAITING;
					tcollider.gameObject.GetComponent<CharacterMotor>().canControl = true;
					if(gameObject.GetComponent<WaypointPath>())
					{
						gameObject.GetComponent<WaypointPath>().enabled = true;
					}
					speechBubble = null;
					if(dialogueMode == DialogueMode.AUTO_ONCE || dialogueMode == DialogueMode.MANUAL_ONCE)
					{
						enabled = false;
					}
				}
			}
		}
	}
	
}
