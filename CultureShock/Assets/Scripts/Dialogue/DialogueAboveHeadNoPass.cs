﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
[SerializeAll]
public class DialogueAboveHeadNoPass : DialogueAboveHead 
{
	public GameObject noPassObject;
	public Vector3 pushDir;
	public float moveSpeed;
	private float oldMoveSpeed;
	private Vector3 dest;
	private bool isMoving = false;
	protected void OnGUI () 
	{
		base.OnGUI();
		if(!isMoving && ds == DialogueState.FINAL)
		{
			//Push back 
			dest = noPassObject.transform.position + pushDir;
			oldMoveSpeed = noPassObject.GetComponent<CharacterMotor> ().movement.maxSidewaysSpeed;
			noPassObject.GetComponent<FPSInputController> ().enabled = false;
			noPassObject.GetComponent<CharacterMotor> ().movement.maxSidewaysSpeed = moveSpeed;
			noPassObject.GetComponent<Attacker>().enabled = false;
			isMoving = true;
		}

		if(isMoving)
		{

			Vector3 moveDirection = dest - noPassObject.transform.position;
			noPassObject.GetComponent<CharacterMotor>().inputMoveDirection = moveDirection.normalized*moveSpeed;
		
			if(moveDirection.magnitude < 2)
			{
				noPassObject.GetComponent<CharacterMotor>().inputMoveDirection = Vector3.zero;
				noPassObject.GetComponent<CharacterMotor> ().movement.maxSidewaysSpeed = oldMoveSpeed;
				noPassObject.GetComponent<FPSInputController> ().enabled = true;
				noPassObject.GetComponent<Attacker>().enabled = true;
				isMoving = false;
				outOfTrigger = true;
				ds = DialogueState.WAITING;
			}
		}
	}
	
}
