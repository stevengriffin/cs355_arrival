
using UnityEngine;
using System.Collections;

public class TextFader : MonoBehaviour 
{
	private bool isStarted = false;
	private Vector3 pos;
	private int textx;
	private int texty;
	private float startTime;
	private float textBoxWidthPercentage = 0.2f;

	public Transform t;
	public float duration;
	public string text;
	public GUIStyle style;
	public Color outColor;
	public Color inColor;

	void Start()
	{
		pos = Camera.main.WorldToViewportPoint(t.position);
		textx = (int)(pos.x*Screen.width) - (int)(textBoxWidthPercentage*Screen.width/2);
		texty = (int)((1-pos.y)*Screen.height) - 200;
		startTime = Time.time;
	}
	
	void OnGUI()
	{

		Color deltaColor = new Color(0,0,0,1.0f)/duration;
		 
		DialogueAboveHead.DrawOutline (new Rect (textx, texty - (50.0f/duration)*(Time.time-startTime), (int)(textBoxWidthPercentage * Screen.width), 100), text, style, outColor, inColor); 
		inColor -= deltaColor*Time.deltaTime;
		outColor -= deltaColor*Time.deltaTime;
		
		if(Time.time > startTime + duration)
			Destroy(gameObject.GetComponent<TextFader>());
	}
	

}
