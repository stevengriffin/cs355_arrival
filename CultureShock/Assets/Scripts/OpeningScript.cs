﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class OpeningScript : MonoBehaviour {
	public GameObject hero;
	public GameObject DockBoundary;
	public Transform heroWp;
	public Transform heroWp2;
	public float moveSpeed;
	public Font defaultFont;
	public float lineDuration;
	public string[] monologue;
	enum OpenScriptState
	{
		MOVE1,
		TALK,
		MOVE2,
		DONE
	};
	private OpenScriptState oss;

	private float timeStart;
	private float oldMoveSpeed;
	private int monologueIdx = 0;
	void Start () {
		if(LevelSerializer.IsDeserializing)
			return;
		oss = OpenScriptState.MOVE1;
		oldMoveSpeed = hero.GetComponent<CharacterMotor> ().movement.maxForwardSpeed;
		hero.GetComponent<FPSInputController> ().enabled = false;
		hero.GetComponent<CharacterMotor> ().movement.maxForwardSpeed = moveSpeed;
		hero.GetComponent<Attacker>().enabled = false;
		Camera.main.GetComponent<CameraCutScene> ().SetCutScene (new Color (0, 0, 0, 1), 25);
		Camera.main.GetComponent<CameraFade> ().SetScreenOverlayColor (new Color (0, 0, 0, 1));
		Camera.main.GetComponent<CameraFade> ().StartFade (new Color (0, 0, 0, 0), 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(oss == OpenScriptState.MOVE1)
		{
			Vector3 moveDirection = heroWp.position - hero.transform.position;
			hero.GetComponent<CharacterMotor>().inputMoveDirection = moveDirection.normalized*moveSpeed;
			
			if(moveDirection.magnitude < 2)
			{
				timeStart = Time.time;
				hero.GetComponent<CharacterMotor>().inputMoveDirection = Vector3.zero;
				oss = OpenScriptState.TALK;
			}
		}
		else if(oss == OpenScriptState.TALK)
		{
			if((timeStart + lineDuration < Time.time))
			{
				timeStart = Time.time;
				monologueIdx++;
			}
			if(monologueIdx == monologue.Length)
				oss = OpenScriptState.MOVE2;
		}
		else if(oss == OpenScriptState.MOVE2)
		{
			Vector3 moveDirection = heroWp2.position - hero.transform.position;
			hero.GetComponent<CharacterMotor>().inputMoveDirection = moveDirection.normalized*moveSpeed;
		
			if(moveDirection.magnitude < 2)
			{
				hero.GetComponent<CharacterMotor>().inputMoveDirection = Vector3.zero;
				hero.GetComponent<CharacterMotor> ().movement.maxForwardSpeed = oldMoveSpeed;
				hero.GetComponent<FPSInputController> ().enabled = true;
				//hero.GetComponent<Attacker>().enabled = true;

				//StartCoroutine(DoCheckpoint());
				

				GUIStyle sty = new GUIStyle();
				sty.fontSize = 15;
				sty.normal.textColor = Color.white;
				sty.wordWrap = true;
				sty.alignment = TextAnchor.LowerCenter;
				
				TextFader tf = gameObject.AddComponent<TextFader> ();
				tf.t = hero.transform;
				tf.duration = 6.0f;
				tf.text = "Use W, A, S, D to move";
				tf.style = sty;
				tf.outColor = Color.black;
				tf.inColor = Color.gray;

				oss = OpenScriptState.DONE;
				Camera.main.GetComponent<CameraCutScene> ().ZoomCutSceneEnd(1.5f);

				DockBoundary.SetActive(true);
			}
		}
	}

	void OnGUI()
	{
		if(oss == OpenScriptState.TALK)
		{
			GUIStyle style = new GUIStyle();
			style.font = defaultFont;
			style.fontSize = 25;
			style.normal.textColor = Color.white;
			style.wordWrap = true;
			style.alignment = TextAnchor.LowerCenter;
			DialogueAboveHead.DrawOutline(GameObject.FindGameObjectWithTag("MainCamera").camera, hero.transform, monologue[monologueIdx], style, Color.black, Color.yellow); 
		}
	}

	private IEnumerator DoCheckpoint()
	{
		LevelSerializer.Checkpoint ();
		yield return 0;
	}

}
