﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Sub_InvItem : SubMission {
	private Inventory inv;
	public List<string> items = new List<string>();
	
	public override void update ()
	{
		/*
		int bingo = 0;
		int total = 0;
		foreach (List<GameObject> type in items)
		{
			total += type.Count;
		}
		
		Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
		foreach (EventItem itemInHand in inv.eventList)
		{
			foreach (List<GameObject> type in items)
			{
				if (itemInHand.title == type[0].name)
				{
					bingo += Mathf.Min(type.Count, itemInHand.invCount);
				}
			}
		}
		if (bingo == total) triggered = true;
		*/
		bool empty = true;
		foreach (Item invitem in inv.eventList)
		{
			for(int i = 0; i < items.Count; i++)
			{
				string item = items[i];

				if(invitem.title == item)
				{
					items.Remove(item);
					break;
				}
			}
		}
		if (items.Count == 0) triggered = true;
	}

	public override void start()
	{
		base.start ();
		started = true;
		inv = GameObject.FindGameObjectWithTag ("Inventory").GetComponent<Inventory> ();
	}

	public void addItem (string name)
	{	
		items.Add(name);
	}
}
