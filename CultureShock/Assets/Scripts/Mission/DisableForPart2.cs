﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class DisableForPart2 : MonoBehaviour {
	GameObject mission;
	public GameObject activate;
	// Use this for initialization
	void Start () {
		mission = GameObject.FindGameObjectWithTag ("Mission");
	}
	
	// Update is called once per frame
	void Update () {
		if(!mission.GetComponent<MissionInterface>().HasActiveMissions())
		{
			activate.SetActive(true);
			gameObject.SetActive(false);
		}
	}
}
