﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Sub_Kill : SubMission {

	public List<GameObject> enemies = new List<GameObject>();
	public List<string> enemyNames = new List<string>();
	public List<Vector3> enemyLocs = new List<Vector3>();
	
	public override void update ()
	{
		List<GameObject> newList = new List<GameObject>();
		for (int i = 0; i < enemies.Count; i++)
		{
			if(enemies[i].GetComponent<EntityStateController>().getState() != null)
			{
				if (enemies[i].GetComponent<EntityStateController>().getState().GetType() == typeof(LiveState))
				{
					newList.Add(enemies[i]);
				}
			}
			else
				return;
		}
		enemies = newList;
		if (started && enemies.Count == 0) triggered = true;
	}

	public override void start()
	{
		base.start ();
		Debug.Log ("start: " + enemyNames.Count + " " + started + " " + enemyNames[0]);
		player = GameObject.FindGameObjectWithTag("Player");
		for(int i = 0; i < enemyNames.Count; i++)
		{
			string pre = "Prefabs/";
			GameObject enemyPrefab = Resources.Load(pre + enemyNames[i]) as GameObject;
			GameObject instance = MonoBehaviour.Instantiate(enemyPrefab, enemyLocs[i], Quaternion.identity) as GameObject;
			instance.GetComponent<MoveTowardsPlayer>().targetTransform = player.transform;
			enemies.Add(instance);
		}
		started = true;
	}

	public override void addKill (string name, Vector3 position)
	{
		enemyNames.Add (name);
		enemyLocs.Add (position);

	}
	
}
