using UnityEngine;
using Serialization;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Mission1 : MainMission {
	Font csf;
	Font df;
	public Mission1()
	{
		csf = Resources.Load<Font> ("Fonts/cultureshock1");
		df = Resources.Load<Font> ("Fonts/Sansation_Light");
		currentStatus = Status.Alive;
		title = "Delivery";
		totalState = 1;
		prizeMoney = 0;
		prizeItem = "Sword";
		prizeExp = 5;
		submissions = new List<SubMission>();
	

		Sub_Dialogue section0 = new Sub_Dialogue();
		GameObject npc = GameObject.FindGameObjectWithTag("Mission1Guard");
		GameObject player = GameObject.FindGameObjectWithTag("Player");

		DialogueLineAbove[] dialogue = new DialogueLineAbove[11];
		dialogue[0] = new DialogueLineAbove(npc.transform, "stop!", csf);
		dialogue[1] = new DialogueLineAbove(player.transform, "What?", df);
		dialogue[2] = new DialogueLineAbove(npc.transform, "you look strong...", csf);
		dialogue[3] = new DialogueLineAbove(npc.transform, "...job for you...", df); 
		dialogue[4] = new DialogueLineAbove(player.transform, "What do you need done? ", df); 
		dialogue[5] = new DialogueLineAbove(npc.transform, "i need to grab...", csf); 
		dialogue[6] = new DialogueLineAbove(npc.transform, "...a package...", df); 
		dialogue[7] = new DialogueLineAbove(npc.transform, "its sitting in the", csf); 
		dialogue[8] = new DialogueLineAbove(npc.transform, "...alleyway...", df); 
		dialogue[9] = new DialogueLineAbove(player.transform, "Hmm, okay I'll find your package.", df); 
		dialogue[10] = new DialogueLineAbove(player.transform, "It must be dangerous back there...", df); 
		section0.SetEnableWaypointOnTrigger (true);
		section0.add(npc, DialogueAboveHead.DialogueMode.AUTO_ONCE, dialogue);
		section0.description = "Talk to the man by the alley.";
		submissions.Add(section0);

		SubMission section1 = new Sub_Kill();
		section1.addKill("EnemyGuard", GameObject.FindGameObjectWithTag("CityEntrance").transform.position + new Vector3(45, 3, 60));
		section1.addKill("EnemyGuard", GameObject.FindGameObjectWithTag("CityEntrance").transform.position + new Vector3(61, 3, 60));
		section1.description = "Kill the thugs.";
		submissions.Add(section1);

		SubMission section2 = new Sub_Item();
		section2.addItem("Package", new Vector3(84.34046f, 1.542929f, -149.0021f));
		section2.description = "Find the package.";
		submissions.Add(section2);

		Sub_Dialogue section3 = new Sub_Dialogue ();
		DialogueLineAbove[] dialogue2 = new DialogueLineAbove[5];
		dialogue2[0] = new DialogueLineAbove(player.transform, "I found your package.", df);
		dialogue2[1] = new DialogueLineAbove(npc.transform, "!", df);
		dialogue2[2] = new DialogueLineAbove(npc.transform, "thank you!", csf);
		dialogue2[3] = new DialogueLineAbove(npc.transform, "...take...", df);
		dialogue2[4] = new DialogueLineAbove(npc.transform, "...this sword...", df);
		section3.SetItemToRemove ("Package");
		section3.add(npc, DialogueAboveHead.DialogueMode.MANUAL_ONCE, dialogue2);
		section3.description = "Bring the package to the man by the alley.";
		submissions.Add(section3);
	}

	void MoveGuard()
	{
		GameObject npc = GameObject.FindGameObjectWithTag("Mission1Guard");
		npc.GetComponent<WaypointPath>().enabled = true;
	}

	void RemovePackage()
	{
		GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().eventList.Remove(Content.fullCategory["Package"]);
	}
}