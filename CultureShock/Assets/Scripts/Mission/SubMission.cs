﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public abstract class SubMission
{

	public delegate void OnTriggerDelegate();

	//[DoNotSerialize]
	protected OnTriggerDelegate onTrigger = null;
	
	public bool started = false;
	// finished?
	public bool triggered = false;
	// text on GUI window
	public string description;
	// where am I
	public int stage = 0;
	// link to parent
	public MainMission body;
	// player object
	public GameObject player;

	public virtual void start ()
	{
		//LevelSerializer.Checkpoint ();
	}

	public abstract void update ();
	
	public virtual void addItem (string name, Vector3 position)
	{}
	
	public virtual void addKill (string name, Vector3 position)
	{}
	
	public virtual void add (GameObject npc, DialogueAboveHead.DialogueMode mode, DialogueLineAbove[] dlines)
	{}
	
}
