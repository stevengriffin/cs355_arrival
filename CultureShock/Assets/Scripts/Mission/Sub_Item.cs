﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Sub_Item : SubMission {

	public List<GameObject> items = new List<GameObject>();
	public List<string> itemNames = new List<string>();
	public List<Vector3> itemLocs = new List<Vector3>();
	
	public override void update ()
	{
		bool empty = true;
		foreach (GameObject item in items)
		{
			if (item != null)
			{
				empty = false;
				break;
			}
		}
		if (started && empty) triggered = true;
	}

	public override void start()
	{
		base.start ();
		started = true;

		for(int i = 0; i < itemNames.Count; i++)
		{
			string pre = "Prefabs/";
			GameObject itemPrefab = Resources.Load(pre + itemNames[i]) as GameObject;
			GameObject instance = MonoBehaviour.Instantiate(itemPrefab, itemLocs[i], Quaternion.identity) as GameObject;
			instance.name = itemNames[i];
			items.Add(instance);
		}
	}

	public override void addItem (string name, Vector3 position)
	{	
		itemNames.Add (name);
		itemLocs.Add (position);
	}

}
