using UnityEngine;
using Serialization;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Mission4 : MainMission {
	Font csf;
	Font df;
	public Mission4()
	{
		csf = Resources.Load<Font> ("Fonts/cultureshock1");
		df = Resources.Load<Font> ("Fonts/Sansation_Light");
		currentStatus = Status.Alive;
		title = "Blang Blang";
		totalState = 1;
		prizeMoney = 1000;
		prizeItem = "";
		prizeExp = 5;
		submissions = new List<SubMission>();
	

		Sub_Dialogue section0 = new Sub_Dialogue();
		GameObject npc = GameObject.FindGameObjectWithTag("Mission4Guy");
		GameObject player = GameObject.FindGameObjectWithTag("Player");

		DialogueLineAbove[] dialogue = new DialogueLineAbove[6];
		dialogue[0] = new DialogueLineAbove(npc.transform, "maybe you can help...", csf);
		dialogue[1] = new DialogueLineAbove(npc.transform, "...peasant...", df);
		dialogue[2] = new DialogueLineAbove(npc.transform, "asshole took it", csf);
		dialogue[3] = new DialogueLineAbove(npc.transform, "stole...wife's necklace", df);
		dialogue[4] = new DialogueLineAbove(player.transform, "You guys look rich.", df);
		dialogue[5] = new DialogueLineAbove(player.transform, "I'll help you out for a price.", df);
		section0.add(npc, DialogueAboveHead.DialogueMode.MANUAL_ONCE, dialogue);
		section0.description = "Talk to the rich guy.";
		submissions.Add(section0);

		Sub_InvItem section2 = new Sub_InvItem();
		section2.addItem("Gold Necklace");
		section2.description = "Find the rich guy's necklace.";
		submissions.Add(section2);

		Sub_Dialogue section3 = new Sub_Dialogue ();
		DialogueLineAbove[] dialogue2 = new DialogueLineAbove[5];
		dialogue2[0] = new DialogueLineAbove(player.transform, "Is this the necklace?", df);
		dialogue2[1] = new DialogueLineAbove(npc.transform, "!", df);
		dialogue2[2] = new DialogueLineAbove(npc.transform, "thank you!", csf);
		dialogue2[3] = new DialogueLineAbove(npc.transform, "...so grateful...", df);
		dialogue2[4] = new DialogueLineAbove(npc.transform, "...take this...", df);
		section3.SetItemToRemove ("Gold Necklace");
		section3.add(npc, DialogueAboveHead.DialogueMode.MANUAL_ONCE, dialogue2);
		section3.description = "Bring the necklace to the rich man.";
		submissions.Add(section3);
	}

	void RemoveNecklace()
	{
		GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().eventList.Remove(Content.fullCategory["Gold Necklace"]);
	}
}