using UnityEngine;
using Serialization;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Mission5 : MainMission {
	Font csf;
	Font df;
	public Mission5()
	{
		csf = Resources.Load<Font> ("Fonts/cultureshock1");
		df = Resources.Load<Font> ("Fonts/Sansation_Light");
		currentStatus = Status.Alive;
		title = "Beat 'Em Up";
		totalState = 1;
		prizeMoney = 400;
		prizeItem = "";
		prizeExp = 15;
		submissions = new List<SubMission>();
	

		Sub_Dialogue section0 = new Sub_Dialogue();
		//GameObject npc = GameObject.FindGameObjectWithTag("Mission5");
		GameObject player = GameObject.FindGameObjectWithTag("Player");

		SubMission section1 = new Sub_Kill();
		section1.addKill("EnemyGuardB", new Vector3(65, 2, -42));
		section1.addKill("EnemyGuard2", new Vector3(70, 2, -42));
		section1.addKill("EnemyGuardB", new Vector3(75, 2, -42));
		section1.addKill("EnemyGuard3", new Vector3(70, 2, -35));
		section1.description = "Kill the gang members and their leader.";
		submissions.Add(section1);

		SubMission section2 = new Sub_Item();
		section2.addItem("Key", new Vector3(70, 2, -35));
		section2.description = "Pickup what the gang dropped.";
		submissions.Add(section2);


	}
}