﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class MainMission
{
	public enum Status
	{
		Waiting,
		Alive,
		Finished,
		Ended,
		Removed
	};

	public MissionInterface missionInterface;

	public string title = "";
	public Status currentStatus = Status.Waiting;
	public int state = 0;
	public int keepsState = 0;
	public int totalState = 2;
	public List<SubMission> submissions;
	
	public const float fade = 5f;
	public const float fadeSpeed = 2f;
	public float time = 0f;
	public int prizeMoney = 0;
	public string prizeItem = "";
	public int prizeExp = 0;
	
	public MainMission()
	{
		submissions = new List<SubMission>(totalState);
		state = 0;
	}
	
	public void update()
	{
		if (currentStatus == Status.Alive)
		{
			if(submissions[state].triggered)
			{
				state++;
				if(state >= submissions.Count)
					state = -1;
			}
			else if(!submissions[state].started)
			{
				submissions[state].start();
			}
		}
		missionInterface = GameObject.FindGameObjectWithTag("Mission").GetComponent<MissionInterface>();
		if (state >= 0) currentStatus = Status.Alive;
		if (state < 0 && currentStatus == Status.Alive)
		{
			currentStatus = Status.Finished;
		}
		
		if (currentStatus == Status.Alive)
		{
			if (keepsState != state)
			{
				keepsState = state;
				missionInterface.popup(title);
			}
			submissions[state].update();
		}
		if (currentStatus == Status.Finished)
		{
			currentStatus = Status.Ended;
			if (prizeExp != 0) 
			{
				givePrizeExp();
			}
			if (prizeMoney != 0) 
			{
				givePrizeMoney();
			}
			if (prizeItem != "") 
			{
				givePrizeItem();
			}


		}
		if (currentStatus == Status.Ended)
		{
			LevelSerializer.Checkpoint();
			Debug.Log("enter ended");
			//GUI.Box(new Rect(Screen.width / 2 - 75f, Screen.height / 2 - 12f, 150f, 24f), "Mission Complete");
			//GUI.Label(new Rect(Screen.width / 2 - 75f, Screen.height / 2 - 10f, 100f, 20f), "Mission Complete");
			//time += Time.deltaTime * fadeSpeed;
			//if (time > fade) 
			missionInterface.remove(this);
			
			currentStatus = Status.Removed;
		}
	}
	
	private int findOngoing()
	{
		for (int i = 0; i < submissions.Count; i++)
		{
			if (!submissions[i].triggered)
			{
				return i;
			}
		}
		return -1;
	}
	
	public void givePrizeMoney()
	{
		Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
		inv.money += prizeMoney;

		GameObject player = GameObject.FindGameObjectWithTag("Player");
		GUIStyle sty = new GUIStyle();
		sty.fontSize = 15;
		sty.normal.textColor = Color.white;
		sty.wordWrap = true;
		sty.alignment = TextAnchor.LowerCenter;
		
		TextFader tf = player.AddComponent<TextFader> ();
		tf.t = player.transform;
		tf.duration = 2.0f;
		tf.text = "Got " + prizeMoney + " dollars";
		tf.style = sty;
		tf.outColor = Color.black;
		tf.inColor = Color.gray;
	}
	
	public void givePrizeExp()
	{
		Debug.Log ("Give exp");
		Experience exp = GameObject.FindGameObjectWithTag("Player").GetComponent<Experience>();
		exp.gainExp(prizeExp);
	}
	
	public void givePrizeItem()
	{
		Content contents = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Content>();
		Item item = contents.getItem(prizeItem);
		if (item != null)
		{
			Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
			inv.get(item);

			GameObject player = GameObject.FindGameObjectWithTag("Player");
			GUIStyle sty = new GUIStyle();
			sty.fontSize = 15;
			sty.normal.textColor = Color.white;
			sty.wordWrap = true;
			sty.alignment = TextAnchor.LowerCenter;
			
			TextFader tf = player.AddComponent<TextFader> ();
			tf.t = player.transform;
			tf.duration = 3.0f;
			tf.text = "Got a " + item.title;
			tf.style = sty;
			tf.outColor = Color.black;
			tf.inColor = Color.gray;
		}
	}
	
	public string getDescription()
	{
		return submissions[state].description;
	}
}
