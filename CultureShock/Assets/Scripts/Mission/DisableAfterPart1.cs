﻿using UnityEngine;
using System.Collections;

public class DisableAfterPart1 : MonoBehaviour {
	GameObject mission;
	// Use this for initialization
	void Start () {
		mission = GameObject.FindGameObjectWithTag ("Mission");
	}
	
	// Update is called once per frame
	void Update () {
		if (!mission.GetComponent<MissionInterface> ().HasActiveMissions ())
			gameObject.SetActive (false);
	}
}
