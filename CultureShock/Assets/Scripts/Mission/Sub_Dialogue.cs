﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class Sub_Dialogue : SubMission {

	public DialogueAboveHead talk;
	private GameObject npc;
	private DialogueAboveHead.DialogueMode mode; 
	private DialogueLineAbove[] dlines;

	private string itemToRemove = "";
	private bool enableWaypointOnTrigger = false;

	public override void start()
	{
		base.start ();
		//if(npc.GetComponent<DialogueAboveHead>())
		//	Object.Destroy(npc.GetComponent<DialogueAboveHead>());
		this.talk = npc.AddComponent<DialogueAboveHead>();
		this.talk.dialogueMode = mode;
		this.talk.dialogue = dlines;
		started = true;
	}

	public override void update ()
	{
		//talk.missionShow();
		if(talk)
		{
			if (talk.GetDialogueState() == DialogueAboveHead.DialogueState.FINAL)
			{
				Object.Destroy(talk);
				if(onTrigger != null)
					onTrigger();
				if(itemToRemove.Length > 0)
					GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().eventList.Remove(Content.fullCategory[itemToRemove]);
				if(enableWaypointOnTrigger)
					npc.GetComponent<WaypointPath>().enabled = true;
				triggered = true;
			}
		}
	}


	
	public override void add (GameObject npc, DialogueAboveHead.DialogueMode mode, DialogueLineAbove[] dlines)
	{
		this.npc = npc;
		this.mode = mode;
		this.dlines = dlines;
	}

	public void SetItemToRemove(string item)
	{
		itemToRemove = item;
	}

	public void SetEnableWaypointOnTrigger(bool enable)
	{
		enableWaypointOnTrigger = enable;
	}
	/*public void SetOnTrigger(OnTriggerDelegate ot)
	{
		this.onTrigger = ot;
	}
	*/
}
