using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
[SerializeAll]
public class TriggeredWaypointThenDialoguePart2 : DialogueAboveHead 
{
	enum WaypointState
	{
		WAITING,
		MOVING,
		DONE
	}
	public Vector3[] waypoints;
	public Transform triggerLoc;
	public Transform spawnLoc;
	public float triggerRad;
	public int speed = 2;
	private WaypointState wps = WaypointState.WAITING;
	private WaypointPath wp;

	protected void OnGUI () 
	{
		if(wps == WaypointState.WAITING)
		{
			Vector3 dir = triggerLoc.position - GameObject.FindGameObjectWithTag("Player").transform.position;
			if(dir.magnitude < triggerRad)
			{
				gameObject.transform.position = spawnLoc.position;
				GameObject.FindGameObjectWithTag("Player").GetComponent<AnimationTriggers>().Enabled(false);
				GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterMotor>().canControl = false;
				wp = gameObject.AddComponent<WaypointPath>();
				wp.waypoints = waypoints;
				wp.speed = speed;
				wp.loop = false;
				Camera.main.GetComponent<CameraCutScene>().ZoomCutSceneStart(3.0f, 25);
				wps = WaypointState.MOVING;
			}
		}
		else if(wps == WaypointState.MOVING)
		{
			if(wp.IsDone())
			{
				wps = WaypointState.DONE;
				TriggerDialogue();
			}
		}
		else if(wps == WaypointState.DONE)
		{
			base.OnGUI();
			if(ds == DialogueState.FINAL)
			{		
				GameObject.FindGameObjectWithTag("Player").GetComponent<AnimationTriggers>().Enabled(true);
				GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterMotor>().canControl = true;
				Camera.main.GetComponent<CameraCutScene>().ZoomCutSceneEnd(3.0f);
				GameObject.FindGameObjectWithTag("Mission").GetComponent<MissionInterface>().add(new Mission5());
				ds = DialogueState.DONE;
			}
		}
	}
	
	public void missionShow()
	{
		this.OnGUI();
	}
	
	
}