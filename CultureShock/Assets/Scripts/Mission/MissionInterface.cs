using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class MissionInterface : MonoBehaviour
{

	public bool isOpen = false;
	public List<MainMission> missions;
	public List<bool> missionDescriptionOn;
	public int offset = 0;
	public Vector2 scrollPosition = Vector2.zero;
	public GameObject missionObject;
	public GUISkin skin;
	// Use this for initialization
	void Start ()
	{
		if (LevelSerializer.IsDeserializing) 
		{
			return;
		}
		missions = new List<MainMission>();
		missionDescriptionOn = new List<bool>();
		missionObject = GameObject.FindGameObjectWithTag("Mission");
		missions.Add(new Mission1());
		missions.Add(new Mission2());
		missions.Add(new Mission3());
		missions.Add(new Mission4());
		missionDescriptionOn.Add(false);
		missionDescriptionOn.Add(false);
		missionDescriptionOn.Add(false);
		missionDescriptionOn.Add(false);
	}

	public bool HasActiveMissions()
	{

		return missions.Count != 0;
	}

	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnGUI()
	{
		if (LevelSerializer.IsDeserializing)
			return;

		GUI.skin = skin;
		//Debug.Log("How many missions: " + missions.Count);
		//Debug.Log(missions[0]);
		// manually update each mission
		if (missions.Count != 0)
		{
			for (int i = 0; i < missions.Count; i++)
			{
				missions[i].update();
			}
		}
		
		if (GUI.Button(new Rect(210, 10, 100, 25), "Mission"))
		{
			isOpen = !isOpen;
			if (isOpen) CloseOtherWindow();
		}
		if (isOpen)
		{
			CloseOtherWindow();
			// check which or if any mission discription is clicked opened, record index
			int index = -1;
			foreach (bool status in missionDescriptionOn)
			{
				if (status) index = missionDescriptionOn.IndexOf(status);
			}
			//Debug.Log("current mission index: " + index);
			// draw things
			GUI.Box(new Rect(10, 35, 280, 200), "");
			this.scrollPosition = GUI.BeginScrollView (new Rect (10,35,280,200), this.scrollPosition, new Rect (0, 0, 200, 200));
			if (index == -1)
			{
				for (int i = 0; i < missions.Count; i++)
				{
					drawPositionBefore(i, missions[i].title);
				}
			}
			else
			{
				for (int i = 0; i < index; i++)
				{
					//Debug.Log("start draw before");
					drawPositionBefore(i, missions[i].title);
					//Debug.Log("End draw before");
				}
				//Debug.Log("start draw current");
				drawOpenPosition(index, missions[index].title, missions[index].getDescription());
				//Debug.Log("end draw current");
				for (int i = index + 1; i < missions.Count; i++)
				{
					//Debug.Log("Start draw after");
					drawPositionAfter(i, missions[i].title);
					//Debug.Log("end draw after");
				}
			}
			GUI.EndScrollView();
		} 
	}
		
	public void add(MainMission newMission)
	{
		missions.Add(newMission);
		missionDescriptionOn.Add(false);
	}
	
	public void CloseOtherWindow()
	{
		Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
		inv.open = false;
		Attribute attri = GameObject.FindGameObjectWithTag("Attribute").GetComponent<Attribute>();
		attri.open = false;
	}
	
	public void drawPositionBefore(int index, string name)
	{
		if (GUI.Button(new Rect(0, index * 32f, 64f, 32f), "+"))
		{
			open(index);
		}
		GUI.Label(new Rect(64f, index * 32f, 216f, 32f), name);
	}
	
	public void drawOpenPosition(int index, string name, string description)
	{
		if (GUI.Button(new Rect(0, index * 32f, 64f, 32f), "-"))
		{
			missionDescriptionOn[index] = false;
		}
		GUI.Label(new Rect(64f, index * 32f, 216f, 32f), name);
		//Debug.Log("before devide");
		List<string> descriptionLines = devideString(description);
		//Debug.Log("After devide");
		for (int i = 0; i < descriptionLines.Count; i++)
		{
			GUI.Label(new Rect(0, index * 32f + 32f + i * 20f, 280, 32), descriptionLines[i]);
		}
		offset = 32 * descriptionLines.Count;
	}
	
	public void drawPositionAfter(int index, string name)
	{
		if (GUI.Button(new Rect(0, index * 32f + offset, 64f, 32f), "+"))
		{
			open(index);
		}
		GUI.Label(new Rect(64f, index * 32f + offset, 216f, 32f), name);
	}
	
	public void open(int index)
	{
		Debug.Log (index);
		for (int i = 0; i < missionDescriptionOn.Count; i++)
		{
			missionDescriptionOn[i] = false;
		}
		missionDescriptionOn[index] = true;
	}
	
	public int getLine(string description)
	{
		return description.Length * 8 / 280;
	}
	
	public List<string> devideString(string description)
	{
		List<string> newlist = new List<string>();
		int devideLength = 280 / 8;
		while (description.Length > devideLength)
		{
			newlist.Add(description.Substring(0, devideLength));
			description = description.Substring(devideLength);
		}
		newlist.Add(description);
		/*
		foreach (string content in newlist)
		{
			Debug.Log(content);
		}
		*/
		return newlist;
	}
	
	public void remove(MainMission missionClass)
	{
		
		int index = 0;
		for (int i = 0; i < missions.Count; i++)
		{
			if (missions[i].title == missionClass.title) 
			{
				index = i;
				break;
			}
		}
		missionDescriptionOn[index] = false;
		missionDescriptionOn.RemoveAt (index);
		missions.RemoveAt(index);
		/*Debug.Log(missionDescriptionOn.Count);
		Debug.Log(missions.Count);
		missionDescriptionOn = new List<bool>(missionDescriptionOn.Count - 1);
		for (int i = 0; i < missionDescriptionOn.Count; i++)
		{
			missionDescriptionOn[i] = false;
		}
		*/
		/*
		foreach (MainMission mission in missions)
		{
			if (mission.title == name)
			{
				missions.Remove(mission);
				break;
			}
		}
		*/
	}
	
	public void popup(string name)
	{
		int index = 0;
		foreach (MainMission mission in missions)
		{
			if (mission.title == name)
			{
				index = missions.IndexOf(mission);
				break;
			}
		}
		open(index);
		isOpen = true;
	}
}

