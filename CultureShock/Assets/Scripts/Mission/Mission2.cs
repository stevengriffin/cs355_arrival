using UnityEngine;
using Serialization;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class Mission2 : MainMission {
	Font csf;
	Font df;
	public Mission2()
	{
		csf = Resources.Load<Font> ("Fonts/cultureshock1");
		df = Resources.Load<Font> ("Fonts/Sansation_Light");
		currentStatus = Status.Alive;
		title = "Medicine Man";
		totalState = 1;
		prizeMoney = 100;
		prizeItem = "";
		prizeExp = 12;
		submissions = new List<SubMission>();
	

		Sub_Dialogue section0 = new Sub_Dialogue();
		GameObject npc = GameObject.FindGameObjectWithTag("Mission2");
		GameObject player = GameObject.FindGameObjectWithTag("Player");

		DialogueLineAbove[] dialogue = new DialogueLineAbove[11];
		dialogue[0] = new DialogueLineAbove(npc.transform, "*Sob sob*...", df);
		dialogue[1] = new DialogueLineAbove(player.transform, "Um...are you okay?", df);
		dialogue[2] = new DialogueLineAbove(npc.transform, "thugs...took my medicine", csf);
		dialogue[3] = new DialogueLineAbove(player.transform, "I can't understand you, do you speak English?", df); 
		dialogue[4] = new DialogueLineAbove(npc.transform, "...", csf); 
		dialogue[5] = new DialogueLineAbove(npc.transform, "those bastards", csf); 
		dialogue[6] = new DialogueLineAbove(npc.transform, "...thugs...", df); 
		dialogue[7] = new DialogueLineAbove(npc.transform, "i cant believe they took it", csf); 
		dialogue[8] = new DialogueLineAbove(npc.transform, "...medicine...", df); 
		dialogue[9] = new DialogueLineAbove(npc.transform, "my family needs it", csf); 
		dialogue[10] = new DialogueLineAbove(npc.transform, "...daughter...", df); 
		//section0.SetOnTrigger (new SubMission.OnTriggerDelegate (RemoveApple));
		section0.add(npc, DialogueAboveHead.DialogueMode.MANUAL_ONCE, dialogue);
		section0.description = "Talk to the crying woman.";
		submissions.Add(section0);

		SubMission section1 = new Sub_Kill();
		section1.addKill("EnemyGuardB", new Vector3(-93, 2, -70));
		section1.addKill("EnemyGuard2", new Vector3(-89, 2, -70));
		section1.addKill("EnemyGuardB", new Vector3(-85, 2, -70));
		section1.description = "Find the thugs and kill them to get the medicine.";
		submissions.Add(section1);

		SubMission section2 = new Sub_Item();
		section2.addItem("Potion", new Vector3(-89, 2, -77));
		section2.description = "Find the medicine.";
		submissions.Add(section2);

		Sub_Dialogue section3 = new Sub_Dialogue ();
		DialogueLineAbove[] dialogue2 = new DialogueLineAbove[5];
		dialogue2[0] = new DialogueLineAbove(player.transform, "I recovered your medicine", df);
		dialogue2[1] = new DialogueLineAbove(npc.transform, "!", df);
		dialogue2[2] = new DialogueLineAbove(npc.transform, "thank you!", csf);
		dialogue2[3] = new DialogueLineAbove(npc.transform, "...take...", df);
		dialogue2[4] = new DialogueLineAbove(npc.transform, "...money...", df);
		section3.SetItemToRemove ("Potion");
		section3.add(npc, DialogueAboveHead.DialogueMode.MANUAL_ONCE, dialogue2);
		section3.description = "Bring the medicine to the woman.";
		submissions.Add(section3);
	}

	void RemoveMedicine()
	{
		GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().eventList.Remove(Content.fullCategory["Potion"]);
	}
}