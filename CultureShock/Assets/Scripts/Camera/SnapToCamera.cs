﻿using UnityEngine;
using System.Collections;

public class SnapToCamera : MonoBehaviour {
	public float distance = 10.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Camera.main.transform.position + new Vector3 (0, 0, distance);
		transform.rotation = Camera.main.transform.rotation;
	}
}
