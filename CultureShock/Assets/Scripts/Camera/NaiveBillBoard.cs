﻿using UnityEngine;
using System.Collections;

public class NaiveBillBoard : MonoBehaviour {

    private readonly Quaternion LOOK_AHEAD = new Quaternion(0, 0, 0, 0);

	void Update () {
        transform.rotation = LOOK_AHEAD;
	}
}
