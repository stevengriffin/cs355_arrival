﻿using UnityEngine;

[SerializeAll]
public class CameraCutScene : MonoBehaviour
{   
	public GameObject mapCam;
	private GUIStyle m_BackgroundStyle = new GUIStyle();		// Style for background tiling
	private GUIStyle m_BackgroundStyle2 = new GUIStyle();		// Style for background tiling
	private Texture2D m_FadeTexture;				// 1x1 pixel texture used for fading
	private Texture2D m_FadeTexture2;				// 1x1 pixel texture used for fading
	private Color m_CurrentScreenOverlayColor = new Color(0,0,0,0);	// default starting color: black and fully transparrent
	private Color m_TargetScreenOverlayColor = new Color(0,0,0,0);	// default target color: black and fully transparrent
	private int m_FadeGUIDepth = -1000;				// make sure this texture is drawn on top of everything
	private FollowPlayer m_fp;
	private float m_distTarget;
	private float m_oldDistTarget;
	private float m_zoomTime;
	private float m_deltaDist;

	// initialize the texture, background-style and initial color:
	private void Awake()
	{		
		m_fp = Camera.main.GetComponent<FollowPlayer> ();
		m_FadeTexture = new Texture2D(1, 1);        
		m_FadeTexture2 = new Texture2D(1, 1);        
		m_BackgroundStyle.normal.background = m_FadeTexture;
		m_BackgroundStyle2.normal.background = m_FadeTexture2;
		SetCutSceneColor(m_CurrentScreenOverlayColor);
		
		// TEMP:
		// usage: use "SetScreenOverlayColor" to set the initial color, then use "StartFade" to set the desired color & fade duration and start the fade
		//SetScreenOverlayColor(new Color(0,0,0,1));
		//StartFade(new Color(1,0,0,1), 5);
	}
	
	
	// draw the texture and perform the fade:
	private void OnGUI()
	{   
		// only draw the texture when the alpha value is greater than 0:
		if (m_CurrentScreenOverlayColor.a > 0)
		{			
			GUI.depth = m_FadeGUIDepth;
			GUI.Label(new Rect(0, 0, Screen.width, Screen.height/6), m_FadeTexture, m_BackgroundStyle);
			GUI.Label(new Rect(0, 5*Screen.height/6, Screen.width, Screen.height/6), m_FadeTexture2, m_BackgroundStyle2);
		}

		if (Mathf.Abs(m_fp.distance - m_distTarget) < Mathf.Abs(m_deltaDist) * Time.deltaTime)
		{
			m_deltaDist = 0;
		}
		else
		{
			m_fp.distance += m_deltaDist * Time.deltaTime;
		}
	}

	public void ZoomCutSceneStart(float zoomTime, float distTarget)
	{
		m_zoomTime = zoomTime;
		m_oldDistTarget = m_fp.distance;
		m_distTarget = distTarget;
		Debug.Log ("Old: " + m_oldDistTarget + " Targ: " + m_distTarget);
		m_deltaDist = (m_distTarget - m_oldDistTarget) / m_zoomTime;
		SetCutSceneColor(new Color(0,0,0,1));

		if(mapCam)
			mapCam.SetActive(false);
	}

	public void ZoomCutSceneEnd(float zoomTime)
	{
		m_zoomTime = zoomTime;
		m_distTarget = m_oldDistTarget;
		m_oldDistTarget = m_fp.distance;
		m_deltaDist = (m_distTarget - m_oldDistTarget) / m_zoomTime;
		SetCutSceneColor(new Color(0,0,0,0));

		if(mapCam)
			mapCam.SetActive(true);

	}

	public void SetCutSceneColor(Color newScreenOverlayColor)
	{
		m_CurrentScreenOverlayColor = newScreenOverlayColor;
		m_FadeTexture.SetPixel(0, 0, m_CurrentScreenOverlayColor);
		m_FadeTexture2.SetPixel(0, 0, m_CurrentScreenOverlayColor);
		m_FadeTexture.Apply();
		m_FadeTexture2.Apply();
	}
	
	// instantly set the current color of the screen-texture to "newScreenOverlayColor"
	// can be usefull if you want to start a scene fully black and then fade to opague
	public void SetCutScene(Color newScreenOverlayColor, float distTarget)
	{
		m_oldDistTarget = m_fp.distance;
		m_distTarget = distTarget;
		m_fp.distance = m_distTarget;
		SetCutSceneColor (newScreenOverlayColor);
		
		if(mapCam)
			mapCam.SetActive(false);
	}
}