﻿using UnityEngine;
using System.Collections;

public class ScreenShaker : MonoBehaviour {

    public float decayFactor = 1.0F;
    private float currentIntensity = 0.0F;
    public Vector3 idealPosition;

	// Use this for initialization
	void Start () {
        idealPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentIntensity > 0.0F)
        {
            transform.position = idealPosition + Random.insideUnitSphere * currentIntensity;
            currentIntensity -= decayFactor * Time.deltaTime;
        }
        else
        {
            transform.position = idealPosition;
        }
	}

    public void Shake(float intensity = 0.5F)
    {
        currentIntensity += intensity;
    }
}