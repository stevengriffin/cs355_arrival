﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public Transform target;
	public float distance = 5.0f;
	public float angle = 0.0f;
    private ScreenShaker shaker;

    void Start () {
        shaker = GetComponent<ScreenShaker>();
	}

	void Update(){
		if (target != null) {
			transform.eulerAngles = new Vector3 (angle, 0, 0);

			shaker.idealPosition = new Vector3 (target.position.x, target.position.y + 1 + distance * Mathf.Sin (angle * Mathf.PI / 180), target.position.z - distance * Mathf.Cos (angle * Mathf.PI / 180));
		}
	}
}

