﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapCamera : MonoBehaviour {

	//private Dictionary<GameObject, GameObject> markerPairs;
	public GUISkin skin;
	private GameObject[] items;
	private GameObject fgs;
	private GameObject player;
	private GameObject egs;
	private GameObject m1;
	private GameObject m2;
	private GameObject playerMarker;
	private Material blue;
	
	// Use this for initialization
	void Start () {
		fgs = GameObject.Find("FriendlyGuard");
		player = GameObject.FindGameObjectWithTag("Player");
		egs = GameObject.Find("EnemyGuard");
		m1 = GameObject.FindGameObjectWithTag("Mission1Guard");
		m2 = GameObject.FindGameObjectWithTag("Mission2");
		playerMarker = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		playerMarker.transform.localScale = new Vector3(5, 0, 5);
		playerMarker.layer = 10;
		playerMarker.renderer.sharedMaterial = loadMaterial("green");
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.eulerAngles = new Vector3(90, 0, 0);
	}
	
	void OnGUI() {
	
		GUI.skin = skin;
		//float w = Screen.width;
		//float h = Screen.height;
		//GUI.Box(new Rect(w * 0.73f, h * 0.68f, w * 0.24f, h * 0.24f), "");
		//GUI.Box(new Rect(w * 0.75f - 5f, h * 0.70f - 4f, w * 0.2f + 10f, h * 0.2f + 8f), "");
		//playerMarker.transform.position = newPosition(player);
		foreach (GameObject marker in GameObject.FindGameObjectsWithTag("Marker"))
		{
			marker.transform.position = newPosition(marker.transform.parent.transform.gameObject);
		}
		
	}
	
	private void buildMarker(GameObject target, string markerName)
	{
		
	}
	
	private Vector3 newPosition(GameObject old)
	{
		Vector3 oldP = old.transform.position;
		return new Vector3(oldP.x, 20, oldP.z);
	}
	
	private Material loadMaterial(string name)
	{
		string path = "Materials/" + name;
		Material ret = Resources.Load(path) as Material;
		Debug.Log(ret);
		return ret;
	}
}
