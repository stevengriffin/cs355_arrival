﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class Team : MonoBehaviour {

    public string teamName = "Enemy";

    public bool OnDifferentTeam(GameObject other)
    {
        return teamName != other.GetComponent<Team>().teamName;
    }
}