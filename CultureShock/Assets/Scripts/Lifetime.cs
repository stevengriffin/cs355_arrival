﻿using UnityEngine;
using System.Collections;

public class Lifetime : MonoBehaviour {

    private readonly double startTime = Time.time;
    public double lifetime = 0.7;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time - startTime > lifetime)
        {
            Destroy(gameObject);
        }
	}
}
