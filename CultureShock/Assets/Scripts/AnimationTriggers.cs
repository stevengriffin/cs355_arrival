﻿using UnityEngine;
using System.Collections;
using Serialization;

[SerializeAll]
public class AnimationTriggers : MonoBehaviour {
	public enum SwingDirection
	{
		SWING_BACK = 1,
		SWING_FRONT,
		SWING_RIGHT,
		SWING_LEFT
	};
	private Animator animator;
	private CharacterMotor cc;
	private const float eps = .01f;
	bool facingFront;
	private SwingDirection swingDirection;

	public bool isSwinger = true;
	// Use this for initialization
	void Start()
	{
		facingFront = false;
		animator = gameObject.GetComponent<Animator>();
		cc = gameObject.GetComponent<CharacterMotor> ();
	}

	public void Enabled(bool e)
	{
		enabled = e;
		animator.SetInteger("Direction", 0);
	}

	// Update is called once per frame
	void Update()
	{
		var vertical = cc.inputMoveDirection.z;
		var horizontal = cc.inputMoveDirection.x;
		//var vertical = Input.GetAxis("Vertical");
		//var horizontal = Input.GetAxis("Horizontal");
		
		if (horizontal > 0 && Mathf.Abs(horizontal) + eps > Mathf.Abs(vertical))
		{
			animator.SetInteger("Direction", 3);
			swingDirection = SwingDirection.SWING_RIGHT;
		}
		else if (horizontal < 0 && Mathf.Abs(horizontal) + eps > Mathf.Abs(vertical))
		{
			animator.SetInteger("Direction", 4);
			swingDirection = SwingDirection.SWING_LEFT;
		}
		else if (vertical > 0)
		{
			animator.SetInteger("Direction", 1);
			swingDirection = SwingDirection.SWING_BACK;
		}
		else if (vertical < 0)
		{
			animator.SetInteger("Direction", 2);
			swingDirection = SwingDirection.SWING_FRONT;
		}
		else 
		{

			animator.SetInteger("Direction", 0);
		}
		if(isSwinger)
			animator.SetInteger("SwingDirection", (int)swingDirection);
		
		if (vertical < 0)
		{
			facingFront = true;
		}
		else if (vertical > 0)
		{
			facingFront = false;
		}
	}

	public SwingDirection GetSwingDirection()
	{
		return swingDirection;
	}

	public bool IsFacingFront()
	{
		return facingFront;
	}
}
