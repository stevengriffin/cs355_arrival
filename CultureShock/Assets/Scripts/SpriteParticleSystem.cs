﻿using UnityEngine;
using System.Collections;

public class SpriteParticleSystem : MonoBehaviour {

	private CoolDownMetro coolDown;
	public float force = 150;
	public double coolDownDuration = 0.3;
	public double particleLifetime = 1.0;
	public Sprite sprite;
	public GameObject parent;
	public bool useGravity = true;
	public Vector3 transformOffset = new Vector3(0, 0, 0);

	// Use this for initialization
	void Start () {
		coolDown = new CoolDownMetro(coolDownDuration);
	}
	
	// Update is called once per frame
	void Update () {
		if (coolDown.Update ()) {
			coolDown.Fire ();
			var particle = new GameObject();
			var lifetime = particle.AddComponent<Lifetime>();
			lifetime.lifetime = particleLifetime;
			var spriteRenderer = particle.AddComponent<SpriteRenderer>();
			spriteRenderer.sprite = sprite;
			var body = particle.AddComponent<Rigidbody>();
			body.useGravity = useGravity;
			body.detectCollisions = false;
			body.AddForce(new Vector3(Random.Range (-force/2, force/2),
			                          Random.Range(0, force),
			              			  Random.Range (-force/2, force/2)));
			if(parent)
			{
				particle.transform.parent = parent.transform;
				particle.transform.position  = parent.transform.position + transformOffset;
			}
		}
	}
}
