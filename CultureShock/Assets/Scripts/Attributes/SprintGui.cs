using UnityEngine;
using System.Collections;

[SerializeAll]
public class SprintGui : BoostGui {
  
    void Start()
    {  
		GUI_WIDTH = 0.1F;
        GUI_HEIGHT = 0.05F;
        GUI_TOP = 0.85F;
    	GUI_LEFT = 0.05F;
    	READY_COLOR = Color.blue;
        PREPPED_COLOR = Color.white;
        UNREADY_COLOR = Color.gray;
        controller = GetComponent<SprintBoostController>();
		message = "Sprint (F)";
	}
}