﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class StunGui : MonoBehaviour {
    private const float GUI_WIDTH = 0.1F;
    private const float GUI_HEIGHT = 0.05F;
    private const float GUI_TOP = 0.9F;
    private const float GUI_LEFT = 0.05F;
    private readonly Color STUN_COLOR = Color.yellow;
    private readonly Color PREPPED_COLOR = Color.white;
    private readonly Color UNREADY_COLOR = Color.gray;
    private StunController stunController;

    void Start()
    {
        stunController = GetComponent<StunController>();
    }

    void OnGUI()
    {
		if (stunController.skill < 1)
		{
			return;
		}
        var canonicalColor = GUI.color;
        if (stunController.Ready())
        {
            GUI.color = stunController.Prepped() ? PREPPED_COLOR : STUN_COLOR;
        }
        else
        {
            GUI.color = UNREADY_COLOR;
        }
        if (GUI.Button(new Rect(GUI_LEFT*Screen.width, GUI_TOP*Screen.height,
            GUI_WIDTH * Screen.width * stunController.GetPercent(), GUI_HEIGHT * Screen.height), "Stun (Q)"))
        {
            stunController.Fire();
        }
        GUI.color = canonicalColor;
    }
}