﻿using UnityEngine;
using System.Collections;

public interface ILevelable {
	void LevelUp();
}

[SerializeAll]
public class Attribute : MonoBehaviour, ILevelable {

	public bool open = false;
	public int attributePointsPerLevelUp = 4;
	public GUISkin skin;
	private GameObject player;
	private int availableAttrPts;

	// Use this for initialization
	void Start () {
		availableAttrPts = 0;
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
			
	}

	public void LevelUp()
	{
		availableAttrPts += attributePointsPerLevelUp;
		Open ();
	}

	public void Open()
	{
		open = true;
		CloseOtherWindow ();
	}
	private void CloseOtherWindow()
	{
		Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
		inv.open = false;
		var missionObject = GameObject.FindGameObjectWithTag("Mission").GetComponent<MissionInterface>();
		missionObject.isOpen = false;
	}
	
	void OnGUI()
	{
		GUI.skin = skin;
		if (GUI.Button(new Rect(110, 10, 100, 25), "Character"))
		{
			open = !open;
			if (open) CloseOtherWindow();
		}
		if (open)
		{
			drawAttribute();		
		}
	}
	
	private void drawAttribute()
	{
		GUI.Box(new Rect(10, 35, 280, 260), "");
		string name = "Name: Hero";
		GUI.Label(new Rect(10, 35, LengthCalc(name), 20), name);

		var ExpInstance = player.GetComponent<Experience>();
		string level = ExpInstance.PrintLevel();
		GUI.Label(new Rect(10, 55, LengthCalc(level), 20), level);
		string exp = ExpInstance.PrintExp();
		GUI.Label(new Rect(10, 75, LengthCalc(exp), 20), exp);
		string neededExp = ExpInstance.PrintNeededExp();
		GUI.Label(new Rect(10, 95, LengthCalc(neededExp), 20), neededExp);

		var healthInstance = player.GetComponent<Health>();
		DrawAttrLabel(115, "HP: " + ((int)healthInstance.health).ToString() 
						+ " / " + ((int)healthInstance.maxHealth).ToString());

		var strengthInstance = player.GetComponent<Strength>();
		DrawAttrLabel(135, "Str: " + strengthInstance.GetStrength());

		var defenseInstance = player.GetComponent<Defense>();
		DrawAttrLabel(155, "Def: " + defenseInstance.defense);

		var hasteInstance = player.GetComponent<HasteBoostController>();
		DrawAttrLabel(175, "Haste: " + hasteInstance.skill);

	    var sprintInstance = player.GetComponent<SprintBoostController>();
		DrawAttrLabel(195, "Sprint: " + sprintInstance.skill);

	    var stunInstance = player.GetComponent<StunController>();
		DrawAttrLabel(215, "Stun: " + stunInstance.skill);


		if(availableAttrPts > 0)
		{	
			if (PlusButton(115)) 
			{
				Level (healthInstance);
			}
			else if (PlusButton(135)) 
			{
				Level (strengthInstance);
			}
			else if (PlusButton(155)) 
			{
				Level (defenseInstance);
			}
			else if (PlusButton(175)) 
			{
				Level (hasteInstance);
			}
			else if (PlusButton(195)) 
			{
				Level (sprintInstance);
			}
			else if (PlusButton(215)) 
			{
				Level (stunInstance);
			}
		
			string attrStr = "Attribute Points Remaining: " + availableAttrPts;
			GUI.Label(new Rect(10, 235, LengthCalc(attrStr), 22), attrStr);
		}

	}

	private static bool PlusButton(int y)
	{
		return GUI.Button (new Rect (200, y, 20, 20), "+"); 
	}

	private void DrawAttrLabel(int y, string attrStr)
	{
		GUI.Label(new Rect(10, y, LengthCalc(attrStr), 20), attrStr);
	}

	private void Level(ILevelable levelable)
	{
		levelable.LevelUp();
		availableAttrPts--;
	}

	private int LengthCalc(string input)
	{
		return input.Length * 8;
	}
}
