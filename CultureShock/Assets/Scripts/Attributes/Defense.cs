﻿using UnityEngine;
using System.Collections;
using System;

[SerializeAll]
public class Defense : MonoBehaviour, ILevelable {

    public int defense { get; private set; }
    public int baseDefense = 0;
    private Armor _armor = new Armor();
    public Armor armor
    {
        get { return _armor; }
        set
        {
            _armor = value;
			if(_armor != null)
           		defense = baseDefense + _armor.defense;
        }
    }

    void Start() {
		if (LevelSerializer.IsDeserializing) 
		{
			return;
		}
        defense = 0;
    }

    void Update()
    {
        var armorComp = GetComponentInChildren<EquippedArmor>();
        if (armorComp)
        {
            armor = armorComp.GetArmor();
        }
    }

    // Applies the defense formula to the amount and returns the result.
    // The defense formula is an easy diminishing returns formula:
    // we don't have to worry about players or enemies becoming too difficult/impossible
    // to damage like we do with flat damage reduction or the D20 system.
    // Some values:
    // 10 defense = 94% of total damage taken
    // 100 defense = 59% of total damage taken
    // 200 defense = 44% of total damage taken
    public double GetAdjustedDamage(double amount)
    {
        return (1 - (1 - Math.Pow(0.99, defense))*0.65)*amount;
    }
    
    public void LevelUp()
    {
    	// defense levelup formula
    	baseDefense += 1 + (int)(baseDefense * 0.2);
    }
}
