﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class HasteGui : BoostGui {
  
    void Start()
    {  
		GUI_WIDTH = 0.1F;
        GUI_HEIGHT = 0.05F;
        GUI_TOP = 0.8F;
    	GUI_LEFT = 0.05F;
    	READY_COLOR = Color.green;
        PREPPED_COLOR = Color.white;
        UNREADY_COLOR = Color.gray;
        controller = GetComponent<HasteBoostController>();
		message = "Haste (E)";
	}
}
