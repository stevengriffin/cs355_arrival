﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class StunController : MonoBehaviour, ILevelable {

    private bool nextAttackStuns = false;
    private CoolDownMetro stunCoolDown;
	public int skill = 0;

    void Start()
    {
        stunCoolDown = new CoolDownMetro(20.0);
    }

	// Update is called once per frame
	void Update() {
        stunCoolDown.Update();
		if (Input.GetKeyDown(KeyCode.Q))
        {
            Fire();
        }
	}

    public bool ShouldStun()
    {
        if (nextAttackStuns)
        {
            nextAttackStuns = false;
            stunCoolDown.Fire();
            return true;
        }
        return false;
    }

    public float GetPercent()
    {
        return stunCoolDown.GetPercent();
    }

    public bool Prepped()
    {
        return nextAttackStuns;
    }

    public bool Fire()
    {
        if (!stunCoolDown.HasFired())
        {
            nextAttackStuns = true;
        }
        return nextAttackStuns;
    }

    public bool Ready()
    {
        return !stunCoolDown.HasFired();
    }

	public void LevelUp()
	{
		skill++;
	}

	public double GetStunDuration()
	{
		return 1.0 + (double) skill*0.1;
	}
}
