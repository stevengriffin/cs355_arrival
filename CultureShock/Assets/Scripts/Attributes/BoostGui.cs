
using UnityEngine;
using System.Collections;

public abstract class BoostGui : MonoBehaviour {
    protected float GUI_WIDTH = 0.1F;
    protected float GUI_HEIGHT = 0.05F;
    protected float GUI_TOP = 0.8F;
    protected float GUI_LEFT = 0.05F;
    protected Color READY_COLOR = Color.green;
    protected Color PREPPED_COLOR = Color.white;
    protected Color UNREADY_COLOR = Color.gray;
	protected string message = "";
    protected BoostController controller;
		
    void OnGUI()
    {   
		if (controller.skill < 1)
		{
			return;
		}
        var canonicalColor = GUI.color;
        if (controller.Ready())
        {
            GUI.color = READY_COLOR;
        }
        else
        {
            GUI.color = controller.IsActive() ? PREPPED_COLOR : UNREADY_COLOR;
        }
        if(GUI.Button(new Rect(GUI_LEFT*Screen.width, GUI_TOP*Screen.height,
            GUI_WIDTH * Screen.width * controller.GetPercent(), GUI_HEIGHT * Screen.height), message))
        {
            controller.Fire();
        }
        GUI.color = canonicalColor;
    }
}