﻿using UnityEngine;
using System.Collections;
using System;

[SerializeAll]
public class Health : MonoBehaviour, ILevelable {

    public double maxHealth = 100.0;
    public double health;
    public double regenRate = 1.0;

	// Use this for initialization
	void Start () {
        health = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        if (health <= 0 && gameObject != null) {
            if (gameObject.tag == "Player")
            {
				GetComponent<DeathScreen>().enabled = true;
            }
            else
            {
            	// player gain exp based onenemy level
            	//var player = GameObject.FindGameObjectWithTag("Player");
            	var exp = gameObject.GetComponent<Experience>();
            	int expMount = (int)Math.Pow(exp.level+0.5f, 2.5) * 2;
				GameObject.FindGameObjectWithTag("Player").GetComponent<Experience>().gainExp(expMount);
            }
			health = 0;
            gameObject.GetComponent<EntityStateController>().Change<DeadState>();
        } else {
            health = regen();
        }
	}

    public double Add(double amount)
    {
        health = Math.Min(maxHealth, health + amount);
        return health;
    }

    public double Decrease(double amount)
    {
        var defense = gameObject.GetComponent<Defense>();
        if (defense)
        {
            health -= defense.GetAdjustedDamage(amount);
        }
        else
        {
            health -= amount;
        }
        return health;
    }

    private double regen()
    {
        return Math.Min(maxHealth, health + regenRate * Time.deltaTime);
    }
    
    public void LevelUp()
    {
    	maxHealth += maxHealth * 0.06;
    }
}
