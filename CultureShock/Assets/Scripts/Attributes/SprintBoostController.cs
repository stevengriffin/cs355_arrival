using UnityEngine;
using System.Collections;

[SerializeAll]
public class SprintBoostController : BoostController {

	private float normalForward;
	private float normalSideways;
	private float normalBack;
	private CharacterMotor motor;

	protected override void Setup()
	{
		code = KeyCode.F;
		motor = gameObject.GetComponent<CharacterMotor>();
		normalForward = motor.movement.maxForwardSpeed;
		normalSideways = motor.movement.maxSidewaysSpeed;
		normalBack = motor.movement.maxBackwardsSpeed;
	}

	private float GetAdjustedSpeed(float normalSpeed)
	{
		return normalSpeed * (1.15f + (float) skill * 0.05f);
	}

    protected override void FireEffect()
	{
    	motor.movement.maxForwardSpeed = GetAdjustedSpeed(normalForward);
		motor.movement.maxSidewaysSpeed = GetAdjustedSpeed(normalSideways);
		motor.movement.maxBackwardsSpeed = GetAdjustedSpeed(normalBack);
		ParticleEffectFactory.CreateParticleSystem(gameObject, "SprintSprite", duration);
	}

	protected override void FinishFiring()
	{
	    motor.movement.maxForwardSpeed = normalForward;
		motor.movement.maxSidewaysSpeed = normalSideways;
		motor.movement.maxBackwardsSpeed = normalBack;
	}
}
