using UnityEngine;
using System.Collections;
using System;

[SerializeAll]
public class Strength : MonoBehaviour, ILevelable {
	
	public int damage { get; private set; }
	public int baseStrength = 1;
	private Weapon _weapon = new Weapon();
	public Weapon weapon
	{
		get { return _weapon; }
		set
		{
			_weapon = value;
		}
	}
	
	void Start() {
		if (LevelSerializer.IsDeserializing) 
		{
			return;
		}
		damage = 1;
	}
	
	void Update()
	{
		var weaponComp = GetComponentInChildren<EquippedWeapon>();
		if (weaponComp)
		{
			weapon = weaponComp.GetWeapon();
		}
	}

	public double GetDamage()
	{
		return 	GetDamageWithWeapon(_weapon);
	}

	public double GetDamageWithWeapon(Weapon w)
	{
		return 	(Math.Pow(1.1, baseStrength)*9) * w.attack;
	}

	public int GetStrength()
	{
		return baseStrength;
	}
	
	public void LevelUp()
	{
		// strength levelup formula
		baseStrength += 1 + (int)(baseStrength * 0.2);
	}
}
