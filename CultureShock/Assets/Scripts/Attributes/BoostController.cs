using UnityEngine;
using System.Collections;

[SerializeAll]
public abstract class BoostController : MonoBehaviour, ILevelable {

    private CoolDownMetro coolDown;
    private CoolDownMetro boost;
	public int skill = 0;
    public double coolDownDuration = 120.0;
    public double duration = 20.0;
	public KeyCode code = KeyCode.E;
	private bool needsReset = false;

	void Start () {
        coolDown = new CoolDownMetro(coolDownDuration);
        boost = new CoolDownMetro(duration);
		Setup();
	}

	protected abstract void Setup ();

	protected abstract void FireEffect ();

	protected abstract void FinishFiring ();

	void Update() {
		if (skill < 1) {
			return;
		}
        coolDown.Update();
        boost.Update();
        if (Input.GetKeyDown(code))
        {
			needsReset = true;
            Fire();
        }
        if (!boost.HasFired() && needsReset)
        {
			needsReset = false;
			FinishFiring();
        }
	}

    public float GetPercent()
    {
        return coolDown.GetPercent();
    }

    public bool IsActive()
    {
        return boost.HasFired();
    }

    public bool Fire()
    {
        if (coolDown.Fire())
        {
            boost.Fire();
			FireEffect ();
            return true;
        }
        return false;
    }

    public bool Ready()
    {
        return !coolDown.HasFired();
    }

	public void LevelUp()
	{
		skill++;
	}
}