﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class HasteBoostController : BoostController {

	private double normalCoolDownDuration;
	private float normalSpeed;
    protected Attacker attacker;

	protected override void Setup()
	{
		code = KeyCode.E;
        attacker = GetComponent<Attacker>();
		normalCoolDownDuration = attacker.GetCoolDownDuration ();
		normalSpeed = gameObject.GetComponent<Animator>().speed;
	}

	private float GetMult()
	{
		return Mathf.Max(0.5f, 0.85f - 0.05f * skill);
	}

	private void SetAnimationSpeed(float newSpeed)
	{
		gameObject.GetComponent<Animator>().speed = newSpeed;
	}

    protected override void FireEffect()
	{
		attacker.SetCoolDownDuration(normalCoolDownDuration*(double) GetMult());
		SetAnimationSpeed(normalSpeed/GetMult());
		ParticleEffectFactory.CreateParticleSystem(gameObject, "HasteSprite", duration);
    }

	protected override void FinishFiring()
	{
	    attacker.SetCoolDownDuration(normalCoolDownDuration);
		SetAnimationSpeed(normalSpeed);
	}
}
