﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class Experience : MonoBehaviour {
    public int level;
	public int attributePointsPerLevelUp = 4;
    public int expNextLevel;
    public int totalExp;
	private GameObject player;

	private bool showLvlUpGUI;
	private int availableAttrPts;
    
    void Start()
    {
		if (LevelSerializer.IsDeserializing) 
		{
			return;
		}
    	level = 1;
		totalExp = 0;
		availableAttrPts = 0;
    	calcExpNextLevel();
		player = GameObject.FindGameObjectWithTag("Player");
		showLvlUpGUI = false;
    }
    
	void OnGUI()
	{
		if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.L) 
		{
			levelUp();
		}
	}

    public string PrintLevel()
    {
    	return "Level: " + level.ToString();
    }
    
    public string PrintExp()
    {
		return "EXP: " + totalExp.ToString() + " / " + expNextLevel.ToString();
    }
    
    public string PrintNeededExp()
    {
		return "Exp to Next Level: " + (expNextLevel - totalExp);
    }
    
    private void levelUp()
    {
    	level++;
		var attr = GameObject.FindGameObjectWithTag ("Attribute").GetComponent<Attribute> ();
		attr.LevelUp(); 
		calcExpNextLevel();	

		GUIStyle sty = new GUIStyle();
		sty.fontSize = 25;
		sty.normal.textColor = Color.white;
		sty.wordWrap = true;
		sty.alignment = TextAnchor.LowerCenter;

		TextFader tf = gameObject.AddComponent<TextFader> ();
		tf.t = gameObject.transform;
		tf.duration = 3.0f;
		tf.text = "Level up!";
		tf.style = sty;
		tf.outColor = Color.black;
		tf.inColor = Color.gray;
    }
    
    public void gainExp(int newExp)
    {
    	totalExp += newExp;
		while (totalExp >= expNextLevel)
    	{
    		levelUp();
    	}
    }
    
	private void calcExpNextLevel()
    {
		expNextLevel = (int)Mathf.Pow((level + 1), 2.5f)*2;
    }
}