﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class HealthBar : MonoBehaviour {
	public Texture healthBarTexture;
	private GameObject healthObj;
	private GUITexture guiTexture;
	private const int borderPix = 5;
    private const float GUI_WIDTH = 0.25F;
    private const float GUI_HEIGHT = 0.025F;
    private const float GUI_TOP = 0.94F;
    private const float GUI_LEFT = 0.68F;
    private readonly Color HEALTH_COLOR = Color.red;
	void Start()
	{
		var healthComponent = gameObject.GetComponent<Health>();
		healthObj = new GameObject ("Health Bar");
		healthObj.transform.position = new Vector3 (0.0f, 0.0f, 0.0f);
		healthObj.transform.localScale = Vector3.zero;
		guiTexture = healthObj.AddComponent<GUITexture> ();
		guiTexture.texture = healthBarTexture;
		guiTexture.border = new RectOffset (borderPix, borderPix, borderPix, borderPix);
		guiTexture.pixelInset = new Rect (GUI_LEFT * Screen.width, GUI_TOP * Screen.height,
		                                  (float)(GUI_WIDTH * Screen.width * (healthComponent.health / healthComponent.maxHealth)), GUI_HEIGHT * Screen.height);
	}

    void OnGUI()
    {
        var healthComponent = gameObject.GetComponent<Health>();
        var canonicalColor = GUI.color;
        GUI.color = HEALTH_COLOR;
		guiTexture.pixelInset = new Rect (GUI_LEFT * Screen.width, GUI_TOP * Screen.height,
		                                       (float)(GUI_WIDTH * Screen.width * (healthComponent.health / healthComponent.maxHealth)), GUI_HEIGHT * Screen.height);
        //GUI.Box(new Rect(GUI_LEFT*Screen.width, GUI_TOP*Screen.height,
        //    (float) (GUI_WIDTH*Screen.width*(healthComponent.health / healthComponent.maxHealth)), GUI_HEIGHT*Screen.height), "Health");
        GUI.color = canonicalColor;
    }
}