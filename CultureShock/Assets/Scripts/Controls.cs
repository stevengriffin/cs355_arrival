﻿
using UnityEngine;
using System.Collections;

public class Controls : MonoBehaviour
{
    bool showHelp = false;
    Vector2 scrollPosition;

    void Start()
    {
        scrollPosition = Vector2.zero;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.H))
        {
            showHelp = !showHelp;
            if (showHelp)
            {
                // Crashes Unity, investigate later.
                Time.timeScale = 0.01f;
            }
            else
            {
                Time.timeScale = 1.0f;
            }
        }
    }

    void OnGUI()
    {
        if (!showHelp)
        {
            GUILayout.BeginArea(new Rect(200, 50, 400, 20));
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Press H for Controls Help");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
            return;
        }

        GUIStyle box = "box";
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 300, 400, 600), box);
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUILayout.Label("W, A, S, D to move.");
        GUILayout.Label("Left-click to interact. Click on an item to pick it up.");
        GUILayout.Label("Enter to initiate and proceed through dialogue.");
        GUILayout.Label("Right-click to attack.");
        GUILayout.Label("Ctrl to stun on next attack.");
        GUILayout.Label("F to activate haste boost.");
        GUILayout.Label("P for pause.");
        GUILayout.Label("H to return to game.");
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}