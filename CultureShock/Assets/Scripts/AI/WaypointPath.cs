﻿using UnityEngine;
using System.Collections;
using Serialization;

[SerializeAll]
public class WaypointPath : MonoBehaviour {
	public Vector3[] waypoints;
	public int speed;
	public bool loop;
	private bool isActive = true;
	private int currentWaypoint;
	// Use this for initialization
	void Start () 
	{
		currentWaypoint = 0;
	}

	public void SetIsActive(bool a)
	{
		isActive = a;
	}

	public bool IsDone()
	{
		if(loop)
			return false;
		return currentWaypoint == waypoints.Length;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 vel = Vector3.zero;
		if(currentWaypoint < waypoints.Length)
		{
			Vector3 target = waypoints[currentWaypoint];
			Vector3 moveDirection = target - transform.position;

			if(moveDirection.magnitude < 1)
			{
				currentWaypoint++;
			}
			else 
			{
				vel = moveDirection.normalized*speed;
			}
		}
		else
		{
			if(loop)
			{
				currentWaypoint = 0;
			}
			else
			{
				vel = Vector3.zero;
			}
		}

		if(!isActive)
		{
			vel = Vector3.zero;
		}
		if(GetComponent<CharacterMotor>())
		{
			GetComponent<CharacterMotor>().inputMoveDirection = vel;
		}
		else if(GetComponent<Rigidbody>())
		{
			rigidbody.velocity = vel;
		}

	}
}
