﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class MoveTowardsPlayer : MonoBehaviour {

    public Transform targetTransform;
    public float forgetfulnessRange = 100.0F;
    private CharacterMotor motor;
    private bool found = false;

	// Use this for initialization
	void Start () {
        motor = GetComponent<CharacterMotor>();
	}
	
	// Update is called once per frame
	void Update () {

		if (!targetTransform || !transform) {
			return;
		}
        var targetPos = targetTransform.position;
        var currPos = transform.position;
        if (Vector3.Distance(targetPos, currPos) < forgetfulnessRange)
        {

        	
        	if (!found) 
        	{
       		 	gameObject.GetComponent<AudioController>().Noticed.Play();
        		found = true;
       		}
       	    motor.inputMoveDirection = new Vector3(targetPos.x - currPos.x, 0, targetPos.z - currPos.z).normalized;
		}
		else
		{
			motor.inputMoveDirection = Vector3.zero;
		}
	}
	
	// Chooses whether the enemy is facing towards or away from the player.
    // Currently only those two directions, probably also left and right in the future.
    public Vector3 GetInitialWeaponPosition()
    {
        var pos = transform.position;
        if (targetTransform.position.z > pos.z)
        {
            return new Vector3(pos.x - 2.0F, pos.y, pos.z);
        }
        return new Vector3(pos.x - 2.0F, pos.y, pos.z);
    }
	public bool IsSwingClockwise()
	{
		var pos = transform.position;
		return targetTransform.position.z < pos.z;
	}
}
