﻿using UnityEngine;
using System.Collections;
using Serialization;

[SerializeAll]
public class DeathScreen : MonoBehaviour {
	public Texture[] deathScreen;
	public float buttonPercWidth;
	public GUISkin titleSkin;
	
	private Vector2 scrollPosition;
	private bool showLoads = false;
	private int curScreen = 0;
	private float timeStart;
	private Color curColor;
	public float screenDuration = .3f;
	public float zoomDist;
	public float zoomTime;

	enum DeathScreenState
	{
		ZOOM,
		FADE,
		ANIM_HOLD,
		ANIM_BLOOD
	};

	private DeathScreenState dss;

	// Use this for initialization
	void OnEnable() 
	{
		curColor = new Color(1f,1f,1f,0f);
		dss = DeathScreenState.FADE;
		timeStart = Time.time;
		GameObject.FindGameObjectWithTag ("PauseMenu").GetComponent<PauseMenu> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(dss == DeathScreenState.ZOOM)
		{
			/*
			if (Mathf.Abs(m_fp.distance - m_distTarget) < Mathf.Abs(m_deltaDist) * Time.deltaTime)
			{
				m_deltaDist = 0;
			}
			else
			{
				m_fp.distance += m_deltaDist * Time.deltaTime;
			}*/
		}
	}

	void OnGUI()
	{
		GUI.skin = titleSkin;
		if(dss == DeathScreenState.FADE)
		{
			Color oldColor = GUI.color;
			GUI.color = curColor;
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), deathScreen[0], ScaleMode.StretchToFill, true, 0);
			GUI.color = oldColor;
			if((timeStart + screenDuration < Time.time))
			{
				curColor += new Color(0f,0f,0f,.05f);
				timeStart = Time.time;
			}
			if(curColor.a >= 1)
			{
				dss = DeathScreenState.ANIM_HOLD;
				timeStart = Time.time;
			}
		}
		else if(dss == DeathScreenState.ANIM_HOLD)
		{
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), deathScreen[0], ScaleMode.StretchToFill, true, 0);
			if((timeStart + 1.5f < Time.time))
			{
				timeStart = Time.time;
				dss = DeathScreenState.ANIM_BLOOD;
			}
		}
		else if(dss == DeathScreenState.ANIM_BLOOD)
		{
			if(curScreen < deathScreen.Length)
			{
			
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), deathScreen[curScreen], ScaleMode.StretchToFill, true, 0);
				if((timeStart + screenDuration < Time.time))
				{
					timeStart = Time.time;
					curScreen++;
				}
			}
			else
			{
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), deathScreen[deathScreen.Length-1], ScaleMode.StretchToFill, true, 0);
				GUILayout.BeginArea(new Rect( Screen.width/2 - 300,Screen.height/2 - 300, 600, 600));
		
				GUILayout.BeginVertical(); 
				GUILayout.FlexibleSpace();
				if (!showLoads) 
				{
					if (GUILayout.Button ("Resume from Last Checkpoint")) 
					{
						LevelSerializer.Resume();
						Time.timeScale = 1;
					} 
					/*else if (GUILayout.Button ("Load Game")) 
					{
						showLoads = true;
					}*/
					else if (GUILayout.Button ("Quit to Title Screen")) 
					{
						Application.LoadLevel (0);
					}
					else if (GUILayout.Button ("Quit to Desktop")) 
					{
						Application.Quit();
					}
				}
				// Load stuff
				if(showLoads)
				{
					if (GUILayout.Button ("Back")) 
					{
						showLoads = false;
					}
					GUILayout.Space(30);
					scrollPosition = GUILayout.BeginScrollView(scrollPosition,GUILayout.Width(600), GUILayout.Height(150));
					foreach(var sg in LevelSerializer.SavedGames[LevelSerializer.PlayerName]) { 
						if(GUILayout.Button(sg.Caption)) { 
						//Application.LoadLevel(1);
							LevelSerializer.LoadSavedLevel(sg.Data);
							Time.timeScale = 1;
						} 
					}
					GUILayout.EndScrollView();
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndVertical();
				GUILayout.EndArea();
			}
		}
	}
}
