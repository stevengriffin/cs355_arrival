﻿using UnityEngine;
using System.Collections;
using Serialization;

public class TitleScreen : MonoBehaviour {
	public Texture titleScreen;
	public float buttonPercWidth;
	public float titlePercHeight;
	public float titlePercWidth;
	public GUISkin titleSkin;
	
	private Vector2 scrollPosition;
	private bool showLoads = false;
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnGUI()
	{
		GUI.skin = titleSkin;
		if (!titleScreen) {
			Debug.LogError("Assign a Texture in the inspector.");
			return;
		}
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), titleScreen, ScaleMode.StretchToFill, true, 0);
		//GUI.Label (new Rect ((int)(((1 - titlePercWidth) / 2) * Screen.width), (int) (titlePercHeight*Screen.height), (int)(titlePercWidth*Screen.width), 100), "culture shock");

		GUILayout.BeginArea(new Rect( Screen.width/2 - 300,Screen.height/2 - 300, 600, 600));
		
		GUILayout.BeginVertical(); 
		GUILayout.FlexibleSpace();
		if (!showLoads) 
		{
			if (GUILayout.Button ("New Game")) 
			{
				Application.LoadLevel (1);
				Time.timeScale = 1;
			} 
			else if (GUILayout.Button ("Resume from Last Checkpoint")) 
			{
				LevelSerializer.Resume();
				Time.timeScale = 1;
			}
			else if (GUILayout.Button ("Quit Game")) 
			{
				Application.Quit();
			}
		}
		// Load stuff
		if(showLoads)
		{
			if (GUILayout.Button ("Back")) 
			{
				showLoads = false;
			}
			GUILayout.Space(30);
			scrollPosition = GUILayout.BeginScrollView(scrollPosition,GUILayout.Width(600), GUILayout.Height(150));
			foreach(var sg in LevelSerializer.SavedGames[LevelSerializer.PlayerName]) { 
				if(GUILayout.Button(sg.Caption)) { 
					//Application.LoadLevel(1);
					LevelSerializer.LoadSavedLevel(sg.Data);
					Time.timeScale = 1;
				} 
			}
			GUILayout.EndScrollView();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.EndArea();

	}
}
