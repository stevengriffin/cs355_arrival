﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[SerializeAll]
public static class ComponentMutator
{
    public static void DisableAllComponentsBut(GameObject parent, Type[] allowedComponents)
    {
        mutateAllComponentsBut(parent, allowedComponents, false);
    }
    
    public static void DisableComponents(GameObject parent, Type[] components)
    {
        mutateComponents(parent, components, false);
    }

    public static void EnableComponents(GameObject parent, Type[] components)
    {
        mutateComponents(parent, components, true);
    }

    private static void mutateComponents(GameObject parent, Type[] components, bool enabled)
    {
        foreach (MonoBehaviour comp in parent.GetComponents<MonoBehaviour>())
        {
            if (comp != null && components.Contains(comp.GetType())) {
                comp.enabled = enabled;
            }
        }
    }
    
    private static void mutateAllComponentsBut(GameObject parent, Type[] allowedComponents, bool enabled)
    {
        foreach (MonoBehaviour comp in parent.GetComponents<MonoBehaviour>())
        {
            if (comp != null && !allowedComponents.Contains(comp.GetType())) {
                comp.enabled = enabled;
            }
        }
    }
}

public interface IEntityState {
	void Start(GameObject parent, EntityStateController controller);
    void Update();
    void HandleHit(double amount);
    void HandleStun(Stunner stunner);
	void HandleKnockback (Vector3 knockback);
}

[SerializeAll]
public class LiveState : IEntityState
{
	private EntityStateController controller;
    private GameObject parent;
    private CoolDownMetro stunnedMetro;
    private readonly Type[] stunnedComponents =
    {
        typeof(CharacterController),
        typeof(CharacterMotor),
    };

	public void Start(GameObject parent, EntityStateController controller)
	{
		this.controller = controller;
        this.parent = parent;
        stunnedMetro = new CoolDownMetro(0.3);
    }

    public void Update()
    {
        if (stunnedMetro.Update())
        {
            wakeUpFromStun();
        }
    }

	public void HandleHit(double amount)
	{
		var healthComp = parent.GetComponent<Health>();
		if (healthComp != null)
		{
			healthComp.Decrease(amount);
			parent.GetComponent<AudioController>().Hit.Play();
			ParticleEffectFactory.CreateBloodSpatter(parent);
			if (parent.tag == "Player")
			{
				GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScreenShaker>().Shake();
			}
		}
	}

    public void HandleStun(Stunner stunner)
    {
        stunnedMetro = new CoolDownMetro(stunner.duration);
        stunnedMetro.Fire();
        ComponentMutator.DisableComponents(parent, stunnedComponents);
    }
	public void HandleKnockback(Vector3 knockback)
	{
		var kb = this.parent.AddComponent<Knockback>();
		kb.knockback = knockback;
		kb.obj = this.parent;
	}

    private void wakeUpFromStun()
    {
        ComponentMutator.EnableComponents(parent, stunnedComponents);
    }
}

[SerializeAll]
public class DeadState : IEntityState
{
	private EntityStateController controller;
    private readonly Type[] allowedComponents =
    {
        typeof(Transform),
        typeof(SpriteRenderer),
        typeof(Animator),
        typeof(AnimationTriggers),
        typeof(EntityStateController),
		typeof(DeathScreen)
    };

	public void Start(GameObject parent, EntityStateController controller)
	{
		this.controller = controller;
		if(parent.GetComponent<Animator>())
			parent.GetComponent<Animator>().SetTrigger("Dead");
        handleAudio(parent);
        dropItems(parent);
        ComponentMutator.DisableAllComponentsBut(parent, allowedComponents);
		if(parent.tag != "Player")
			controller.DestroyObjectTimer (parent, 2.0f);
    }

    private void handleAudio(GameObject parent)
    {
        var audioController = parent.GetComponent<AudioController>();
        audioController.StopAll();
        audioController.Death.Play();
    }

    private void dropItems(GameObject parent)
    {
        dropIfExists(parent.GetComponentInChildren<EquippedArmor>(), parent);
        dropIfExists(parent.GetComponentInChildren<EquippedWeapon>(), parent);
    }

    private void dropIfExists(IDroppable droppable, GameObject parent)
    {
        if (droppable != null)
        {
            droppable.Drop(parent);
        }
    }

    public void Update()
    {

    }

    public void HandleHit(double amount)
    {
        // Dead entities do nothing when hit for now.
    }

    public void HandleStun(Stunner stunner)
    {
        // Dead entities do nothing when stunned.
    }

	public void HandleKnockback(Vector3 knockback)
	{

	}
}