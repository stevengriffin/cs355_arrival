﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]

public class Shop : MonoBehaviour 
{
	public string content;
	public DialogueLineAbove[] dialogue;
	public double textBoxWidthPercentage = .4;
	public float speechBubbleOffsetX;
	public float speechBubbleOffsetY;
	public Font defaultFont;
	public const float height = 395f;
	public const float width = 320f;
	public Rect menuArea;
	public const float buttonHeight = 25f;
	public const float columnHeight = 64f;
	public GUISkin skin;
	//public GameObject picObject;
	public int shopTiming;
	public bool autoTrigger = true;
	private bool shopOn = true;
	private Vector2 scrollPosition = Vector2.zero;
	private Inventory inventory;
	private GameObject user;
	
	public List<Item> itemForSell;
	/*
	public List<Item> armorForSell;
	public List<Item> weaponForSell;
	public List<Item> eventItemForSell;
	public List<Item> foodForSell;
	*/
	//public int categoryCount;
	
	enum dialogueState
	{
		WAITING,
		IN_RANGE,
		TALKING,
		SHOPPING
	};
	private int curLine = 0;
	private bool outOfTrigger = true;
	private Collider tcollider;
	private GUIStyle style;
	private GameObject speechBubble;
	private dialogueState ds = dialogueState.WAITING;

	void Start()
	{
		itemForSell = new List<Item>();
		user = GameObject.FindGameObjectWithTag("Player");
		inventory = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
		menuArea = new Rect((Screen.width - width)/2, (Screen.height - height)/2, width+20, height);
		loadContent();
	}
	
	private void loadContent()
	{
		string[] contents = content.Split(',');
		foreach (string title in contents)
		{
			if (Content.fullCategory.ContainsKey(title))
			{
				itemForSell.Add((Item)Content.fullCategory[title]);
			}
		}
	}
/*
	public static Shop CreateComponent(GameObject where, List<Item> wList, List<Item> aList, List<Item> fList, List<Item> eList)
	{
		Shop newShop = where.AddComponent<Shop>();
		newShop.weaponForSell = wList;
		newShop.armorForSell = aList;
		newShop.foodForSell = fList;
		newShop.eventItemForSell = eList;
		newShop.menuArea = new Rect((Screen.width - width)/2, (Screen.height - height)/2, width, height);
		return newShop;
	}
*/

	void OnTriggerEnter ( Collider collider ) 
	{
		if (collider.CompareTag("Player") && ds == dialogueState.WAITING && outOfTrigger) 
		{
			tcollider = collider;
			ds = dialogueState.IN_RANGE;
			
			/*
			speechBubble = (GameObject)Resources.Load("Prefabs/SpeechBubble");
			if(speechBubble)
			{
				//Debug.Log("Could find SpeechBubble resource.");
				speechBubble.transform.parent = this.gameObject.transform;
				speechBubble.transform.localScale = new Vector3(0,0,0);
				speechBubble.transform.localPosition = new Vector3(speechBubbleOffsetX,speechBubbleOffsetY,0);
				speechBubble.animation.CrossFadeQueued("SpeechBubble_Pulse");
			}
			
			else
			{
				Debug.Log("Could not find SpeechBubble resource.");
			}
			*/
			
			
			outOfTrigger = false;
			if(autoTrigger)
			{
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = false;
				ds = dialogueState.TALKING;
			}
		}
	}
	
	void OnTriggerExit( Collider collider){
		if (collider.CompareTag ("Player")) {
			outOfTrigger = true;
			if(ds == dialogueState.IN_RANGE)
			{
				//speechBubble.animation.Play("SpeechBubble_Close");
				//DestroyObject(speechBubble,1);
			}
			ds = dialogueState.WAITING;
		}
	}
	
	void OnGUI () {
		GUI.skin = skin;
		if (ds == dialogueState.WAITING)
		{
			// show money bag on head
		}
		if (ds == dialogueState.IN_RANGE) 
		{
			if(Event.current.type == EventType.MouseDown && Event.current.button == 0)
			{
				//speechBubble.animation.Play("SpeechBubble_Close");
				//DestroyObject(speechBubble,1);
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = false;
				
				//style = new GUIStyle ();
				//style.fontSize = 20;
				
				ds = dialogueState.TALKING;
			}
		}
		else if (ds == dialogueState.TALKING) 
		{
			if (curLine >= dialogue.Length) 
			{
				curLine = 0;
				ds = dialogueState.WAITING;
				tcollider.gameObject.GetComponent<CharacterMotor>().canControl = true;
				speechBubble = null;
				shopOn = true;
			}
			else
			{
				Vector3 pos = Camera.main.WorldToViewportPoint(dialogue[curLine].character.position);
				int textx = (int)(pos.x*Screen.width) - (int)(textBoxWidthPercentage*Screen.width/2);
				int texty = (int)((1-pos.y)*Screen.height) - 200;
	
				style = new GUIStyle();
				style.font = defaultFont;
				style.fontSize = 25;
				//style.fontStyle = FontStyle.Bold;
				style.normal.textColor = Color.white;
				style.wordWrap = true;
				style.alignment = TextAnchor.LowerCenter;
				//GUI.Box(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width),Screen.height-120,(int)(textBoxWidthPercentage*Screen.width),110),"");
				//GUI.Box(new Rect(textx,texty,(int)(textBoxWidthPercentage*Screen.width),110),"");
				//GUI.Label(new Rect((int)(((1+textBoxWidthPercentage)/2)*Screen.width) - 140,Screen.height-40,(int)(textBoxWidthPercentage*Screen.width)-15,100), "Click to continue...", style);
				//GUI.Label(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width) + 15,Screen.height-100,(int)(textBoxWidthPercentage*Screen.width)-15,100),dialogue[curLine].charName + ": " ,style);
				//GUI.Label(new Rect(textx + 15, texty,(int)(textBoxWidthPercentage*Screen.width)-15,100),dialogue[curLine].charName + ": " ,style);
				style.font = dialogue[curLine].font;
				style.richText = dialogue[curLine].richText;
				//GUI.Label(new Rect((int)(((1-textBoxWidthPercentage)/2)*Screen.width) + 15 + dialogue[curLine].lineOffset,Screen.height-100,(int)(textBoxWidthPercentage*Screen.width)-20-dialogue[curLine].lineOffset,100), dialogue[curLine].dialogueLine, style);
				Color inColor = dialogue[curLine].character.tag == "Player" ? Color.yellow : Color.white;
				DialogueAboveHead.DrawOutline(new Rect(textx, texty,(int)(textBoxWidthPercentage*Screen.width),100), dialogue[curLine].dialogueLine, style, Color.black, inColor);
			}
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0) 
			{ 
				curLine++;
				if (curLine == shopTiming && shopOn == true)
				{
					ds = dialogueState.SHOPPING;
				}
				else if (curLine >= dialogue.Length) 
				{
					curLine = 0;
					ds = dialogueState.WAITING;
					tcollider.gameObject.GetComponent<CharacterMotor>().canControl = true;
					speechBubble = null;
					shopOn = true;
				}
			}

		}
		else if (ds == dialogueState.SHOPPING)
		{
			drawShopping();
		}
	}

	private Rect getTotalArea(int counter) {return new Rect(0, counter * 64, width, columnHeight);}
	private Rect getPicArea(int counter) {return new Rect(0, counter * 64, columnHeight, columnHeight);}
	private Rect getTipArea(int counter) {return new Rect(columnHeight, counter * 64 - 8, 128, columnHeight / 2 + 8);}
	private Rect getPriceArea(int counter) {return new Rect(columnHeight, counter * 64 + columnHeight / 2, 128, columnHeight / 2);}
	private Rect getUpButtonArea(int counter) {return new Rect(192, counter * 64, columnHeight, columnHeight / 2);}
	private Rect getDownButtonArea(int counter) {return new Rect(192, counter * 64 + columnHeight / 2, columnHeight, columnHeight / 2);}
	private Rect getCountArea(int counter) {return new Rect(256, counter * 64, columnHeight, columnHeight / 2);}
	private Rect getHadArea(int counter) {return new Rect(256, counter * 64 + columnHeight / 2, columnHeight, columnHeight / 2);}

	private void drawButton(Item i, int counter)
	{
		GUI.Box(getTotalArea(counter), "");
		var centeredStyle = GUI.skin.GetStyle("Box");
		centeredStyle.alignment = TextAnchor.MiddleCenter;
		GUI.Box(getPicArea(counter), i.pic, centeredStyle);
		GUI.Label(getTipArea(counter), i.GetToolTip(user));
		GUI.Label(getPriceArea(counter), i.price.ToString());
		if (GUI.Button(getUpButtonArea(counter), "+"))
		{
			i.shopCount = Math.Min(i.shopCount + 1, 99);
		}
		else if (GUI.Button(getDownButtonArea(counter), "-"))
		{
			i.shopCount = Math.Max(i.shopCount - 1, 0);
		}
		string countString = "Buy: " + i.shopCount.ToString();
		GUI.Label(getCountArea(counter), countString);
		string hadString = "Had: " + i.invCount.ToString();
		GUI.Label(getHadArea(counter), hadString);
	}
			
	private void drawShopping()
	{
		GUI.BeginGroup(menuArea);
		GUI.Box(new Rect(0, 0, width, height), "");
		if (GUI.Button(new Rect(0, 0, LengthCalc("Leave") + 10, buttonHeight), "Leave"))
		{
			shopOn = false;
			ds = dialogueState.TALKING;
		}
		int currentMoney = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>().money;
		string money = "Current Money: " + currentMoney.ToString();
		GUI.Label(new Rect(width - LengthCalc(money), 0, LengthCalc(money), buttonHeight), money);
		
		int counter = 0;
		int total = 0;		
		scrollPosition = GUI.BeginScrollView(new Rect(0, 50, width+20, 320), scrollPosition, new Rect(0, 0, width, 64*itemForSell.Count), false, true);
		foreach (Item i in itemForSell)
		{
			drawButton(i, counter);
			total += i.price * i.shopCount;
			counter++;
		}
		GUI.EndScrollView();
		
		string totalPrice = "Total: " + total.ToString();
		var originalColor = GUI.color;
		if (total > currentMoney) GUI.color = Color.red;
		GUI.Label(new Rect(0, 362, LengthCalc(totalPrice), 25), totalPrice);
		GUI.color = originalColor;
		
		if (GUI.Button(new Rect(width - buttonHeight - 10, 370, buttonHeight + 10, buttonHeight), "Buy"))
		{
			if (total > currentMoney)
			{
				// bubu
			}
			else
			{
				inventory.money -= total;
				foreach (Item i in itemForSell)
				{
					if (i.shopCount > 0)
					{
						for (int index = 0; index < i.shopCount; index++)
						{
							inventory.get(i);
						}
						i.shopCount = 0;
					}
				}
				shopOn = false;
				ds = dialogueState.TALKING;
			}
		}
		GUI.EndGroup();
	}
	
	private int LengthCalc(string input)
	{
		return input.Length * 8;
	}
}
