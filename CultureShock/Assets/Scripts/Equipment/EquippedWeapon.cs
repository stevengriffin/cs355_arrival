﻿using UnityEngine;
using System.Collections;
using Serialization;

// Wrapper script to let us change equipped weapons of enemies in the editor
[SerializeAll]
public class EquippedWeapon : MonoBehaviour, IDroppable {

    private Weapon weapon;
    // Unity doesn't seem to allow us to use properties in the editor, so we have to update the weapon each update.
    //public int attack = 0;
    //public float length = 2.0F;
    public string title = "";
    //public Texture pic = null;
	
	void Start (){
		UpdateWeapon ();
	}
	
	void Update () {
		if(weapon == null)
		{
			UpdateWeapon();
		}
		if (weapon != null && weapon.sprite == null)
		{
			weapon.ReloadSprite ();
			SetWeapon(weapon);
		}
        //weapon.attack = attack;
        //weapon.title = title;
        //weapon.pic = pic;
	}

	public void UpdateWeapon()
	{
		if(title != "")
		{
			Item value;
			if (Content.fullCategory.TryGetValue(title, out value))
			{
				SetWeapon (value as Weapon);
			}
		}
	}

    // In case someone wants to set a weapon programmatically
    public void SetWeapon(Weapon newWeapon)
    {
        weapon = newWeapon;
        //attack = weapon.attack;
        title = weapon.title;
        //pic = weapon.pic;
		Debug.Log (weapon.sprite);
		if (gameObject.GetComponent<SpriteRenderer> ())
			gameObject.GetComponent<SpriteRenderer> ().sprite = weapon.sprite;
    }

	public Weapon GetWeapon()
	{
		return weapon;
	}

    public void Drop(GameObject parent)
    {
		GameObject obj = Instantiate (Resources.Load("Prefabs/" + weapon.title), parent.transform.position + Random.onUnitSphere*3 + new Vector3(0,3,0), Quaternion.identity) as GameObject;
		obj.name = weapon.title;
    }
}