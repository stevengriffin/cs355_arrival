﻿using UnityEngine;
using System.Collections;

public interface IDroppable {
    void Drop(GameObject parent);
}
