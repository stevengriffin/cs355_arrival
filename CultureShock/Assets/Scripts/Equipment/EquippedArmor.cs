﻿using UnityEngine;
using System.Collections;
using Serialization;

// Wrapper script to let us change equipped weapons of enemies in the editor
[SerializeAll]
public class EquippedArmor : MonoBehaviour, IDroppable {

    private Armor armor;
    // Unity doesn't seem to allow us to use properties in the editor, so we have to update the armor each update.
    //public int defense = 0;
    public string title = "";
    //public Texture pic = null;
	
	void Start () {
		Item value;
		if (Content.fullCategory.TryGetValue(title, out value))
		{
			SetArmor (value as Armor);
		}
	}
	
	void Update () {
		if(armor == null)
		{
			Item value;
			if (Content.fullCategory.TryGetValue(title, out value))
			{
				SetArmor (value as Armor);
			}
		}
	}
	
    // In case someone wants to set a armor programmatically
    public void SetArmor(Armor newArmor)
    {
        armor = newArmor;
        //defense = armor.defense;
        title = armor.title;
        //pic = armor.pic;

    }

    public Armor GetArmor()
    {
        return armor;
    }

    public void Drop(GameObject parent)
    {
		GameObject obj = Instantiate (Resources.Load("Prefabs/" + armor.title), parent.transform.position + Random.onUnitSphere*3 + new Vector3(0,3,0), Quaternion.identity) as GameObject;
		obj.name = armor.title;
    }
}