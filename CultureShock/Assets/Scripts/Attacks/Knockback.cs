using UnityEngine;
using System.Collections;

public class Knockback : MonoBehaviour 
{
	private bool isStarted = false;
	private float knockbackDuration = 0.5f;
	private float knockbackStop = 0.2f;
	public Vector3 knockback;
	public GameObject obj;
	void Start()
	{
	}

	void Update()
	{
		if(!isStarted)
		{
			isStarted = true;
			StartCoroutine(DoKnockback(knockback,obj));
		}
	}
	private IEnumerator DoKnockback(Vector3 knockback, GameObject obj)
	{
		if(obj.GetComponent<CharacterController>())
		{   
			if(obj.GetComponent<MoveTowardsPlayer>())
				obj.GetComponent<MoveTowardsPlayer>().enabled = false;
			if(obj.GetComponent<Attacker>())
				obj.GetComponent<Attacker>().enabled = false;
			obj.GetComponent<CharacterMotor>().enabled = false;
			var startTime = Time.time;
			while(Time.time < startTime + knockbackDuration)
			{			
				obj.GetComponent<CharacterController>().SimpleMove(knockback);
				yield return 0;
			}
			while(Time.time < startTime + knockbackDuration + knockbackStop)
			{			
				//obj.GetComponent<CharacterController>().SimpleMove(Vector3.zero);
				yield return 0;
			}
		}
		if(obj.GetComponent<Attacker>())
			obj.GetComponent<Attacker>().enabled = true;
		//obj.GetComponent<CharacterController>().enabled = true;
		obj.GetComponent<CharacterMotor>().enabled = true;
		if(obj.GetComponent<MoveTowardsPlayer>())
			obj.GetComponent<MoveTowardsPlayer> ().enabled = true;
		Destroy(obj.GetComponent<Knockback>());
		yield return 0;
	}

}