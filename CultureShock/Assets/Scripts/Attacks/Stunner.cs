﻿using UnityEngine;
using System.Collections;

// Allows the attack to stun entities it hits
public class Stunner : MonoBehaviour {
    public double duration = 1.0;
}