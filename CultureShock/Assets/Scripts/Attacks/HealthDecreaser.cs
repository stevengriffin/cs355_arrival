﻿using UnityEngine;
using System.Collections;

// This class is handling not just health but all the collision stuff:
// health, playing a sound, and making blood. I'll hopefully split it up or change the
// name someday.
public class HealthDecreaser : MonoBehaviour {
	public float knockbackSpeed = 18.0f;
    public double amount = 0.0;
    // Initiator of attack
    public GameObject parent;
    // null if the attack doesn't stun
    private Stunner stunner;

	// Use this for initialization
	void Start () {
        stunner = GetComponent<Stunner>();    
    }

    void OnTriggerEnter(Collider collider)
    {
        var team = collider.gameObject.GetComponent<Team>();
        if (team && team.OnDifferentTeam(parent))
        {
			Vector3 knockback = collider.gameObject.transform.position - parent.transform.position;
			knockback = knockback.normalized*knockbackSpeed;

			collider.gameObject.GetComponent<EntityStateController>().HandleHit(amount, stunner, knockback);
		}
    }




}
