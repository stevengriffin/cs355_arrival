﻿using UnityEngine;
using System.Collections;

public abstract class Attacker : MonoBehaviour {

    private CoolDownMetro coolDown;
    private AttackFactory attackMaker;
	private double initCoolDown;
	private const float INIT_PITCH = 1.0f;
    public double coolDownDuration = 1.0;

	// Use this for initialization
	void Start () {
        attackMaker = new AttackFactory();
        coolDown = new CoolDownMetro(coolDownDuration);
		initCoolDown = coolDownDuration;
	}
	
	// Update is called once per frame
	void Update () {
        coolDown.Update();
        // Firing is short-circuited.
        if (ShouldAttack() && coolDown.Fire())
        {
			Attack ();
        }
	}

	public void Attack()
	{
		gameObject.GetComponent<AudioController>().Swing.Play();
		//if(gameObject.tag == "Player")
		{
			gameObject.GetComponent<Animator>().SetTrigger("Swing");
		}
		attackMaker.Create(gameObject);
	}
	
	// e.g. enemy is close to player or player clicked mouse
    public abstract bool ShouldAttack();

    public void SetCoolDownDuration(double newDuration)
    {
        coolDownDuration = newDuration;
        coolDown.duration = coolDownDuration;
		gameObject.GetComponent<AudioController>().Swing.pitch = INIT_PITCH*((float) (initCoolDown / newDuration));

    }

    public double GetCoolDownDuration()
    {
        return coolDownDuration;
    }
}
