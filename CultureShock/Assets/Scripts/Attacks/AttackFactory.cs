﻿using UnityEngine;
using System.Collections;

public class AttackFactory {

    private Transform transform;
    private Vector3 ATTACK_SIZE = new Vector3(2.0F, 0.3F, 0.3F);
    private const float BASE_SWING_SPEED = 200.0F;

    public GameObject Create(GameObject attacker)
    {
        if (attacker == null)
        {
            throw new UnassignedReferenceException("Tried to attack with a null attacker");
        }
        var attack = GameObject.CreatePrimitive(PrimitiveType.Cube);
        // Uncomment when attacking sprite gets added
        Object.Destroy(attack.GetComponent<MeshRenderer>());

        var transform = attack.GetComponent<Transform>();
        transform.parent = attacker.GetComponent<Transform>();
        var pos = transform.parent.position;
        var moveTowards = attacker.GetComponent<MoveTowardsPlayer>();
		var animationTrigs = attacker.GetComponent<AnimationTriggers> ();
        /*if (moveTowards)
        {
            transform.position = moveTowards.GetInitialWeaponPosition();
        }
        else*/ if(animationTrigs)
        {
			if(animationTrigs.GetSwingDirection() == AnimationTriggers.SwingDirection.SWING_LEFT || animationTrigs.GetSwingDirection() == AnimationTriggers.SwingDirection.SWING_RIGHT)
			{
				transform.position = new Vector3(pos.x, pos.y, pos.z + 2.0F);
			}
			else 
			{
				transform.position = new Vector3(pos.x - 2.0F, pos.y, pos.z);
			}
        }
        float swingDuration = (float) attacker.GetComponent<Attacker>().GetCoolDownDuration() * 0.5F;
		var lifetime = attack.AddComponent<Lifetime>();
        lifetime.lifetime = swingDuration;
		var rb = attack.AddComponent<Rigidbody> ();
		rb.useGravity = false;
        var hd = attack.AddComponent<HealthDecreaser>();
        hd.parent = attacker;
        float weaponLength;
		var strengthComp = attacker.GetComponent<Strength>();
		if(strengthComp)
		{
			hd.amount += strengthComp.GetDamage();
		}
        var weapon = attacker.GetComponentInChildren<EquippedWeapon>();
        if (weapon) {
            weaponLength = weapon.GetWeapon().length;
        }
        else
        {
            weaponLength = ATTACK_SIZE.x;
        }
        var parentScale = attacker.transform.localScale;
		/*if(moveTowards)
		{
			transform.localScale = new Vector3(weaponLength / parentScale.x, ATTACK_SIZE.y / parentScale.y, ATTACK_SIZE.z / parentScale.z);
		}
		else*/ if(animationTrigs)
		{
			if(animationTrigs.GetSwingDirection() == AnimationTriggers.SwingDirection.SWING_LEFT || animationTrigs.GetSwingDirection() == AnimationTriggers.SwingDirection.SWING_RIGHT)
			{
				transform.localScale = new Vector3(ATTACK_SIZE.z / parentScale.x, ATTACK_SIZE.y / parentScale.y, weaponLength / parentScale.z);
			}
			else
			{
        		transform.localScale = new Vector3(weaponLength / parentScale.x, ATTACK_SIZE.y / parentScale.y, ATTACK_SIZE.z / parentScale.z);
			}
		}
		
		attack.collider.isTrigger = true;
        var swinger = attack.AddComponent<Swinger>();
        swinger.swingSpeed = BASE_SWING_SPEED / swingDuration;
		/*if (moveTowards)
		{
			swinger.clockwise = moveTowards.IsSwingClockwise();
		}
		else */if(animationTrigs)
		{
			if(animationTrigs.GetSwingDirection() == AnimationTriggers.SwingDirection.SWING_LEFT)
				swinger.clockwise = true;
			else if(animationTrigs.GetSwingDirection() == AnimationTriggers.SwingDirection.SWING_RIGHT)
				swinger.clockwise = false;
			else
				swinger.clockwise = animationTrigs.IsFacingFront();
		}
		var stunController = attacker.GetComponent<StunController>();
        if (stunController && stunController.ShouldStun())
        {
            var stunner = attack.AddComponent<Stunner>();
            stunner.duration = stunController.GetStunDuration();
        }
		//ParticleEffectFactory.CreateAttackSystem(attack);
        return attack;
    }
}