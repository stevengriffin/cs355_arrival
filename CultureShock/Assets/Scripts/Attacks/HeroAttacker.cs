﻿using UnityEngine;
using System.Collections;

public class HeroAttacker : Attacker {

    private const int LEFT_MOUSE = 0;
    private const int RIGHT_MOUSE = 1;
    
    public override bool ShouldAttack() {
        return Input.GetMouseButtonDown(RIGHT_MOUSE);
    }
}
