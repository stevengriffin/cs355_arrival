﻿using UnityEngine;
using System.Collections;

public class EnemyAttacker : Attacker {

    // TODO Determine based on length of weapon
    private const float MIN_DIST_TO_MELEE = 10.0F;

    // Attack if player is within range
    public override bool ShouldAttack() {
        var player = GameObject.FindGameObjectWithTag("Player");
        if (player != null) {
            return Vector3.Distance(gameObject.transform.position, player.transform.position) <= MIN_DIST_TO_MELEE;
        }
        return false;
    }
}