﻿using UnityEngine;
using System.Collections;

public class Swinger : MonoBehaviour {

    public float swingSpeed = 600.0F;
	public bool clockwise = false;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
		if(clockwise)
			transform.RotateAround(transform.parent.position, Vector3.down, Time.deltaTime * swingSpeed);
		else
			transform.RotateAround(transform.parent.position, Vector3.up, Time.deltaTime * swingSpeed);
	}
}
