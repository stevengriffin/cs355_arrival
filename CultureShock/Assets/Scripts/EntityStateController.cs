﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class EntityStateController : MonoBehaviour {

    private IEntityState state;

	// Use this for initialization
	void Start () {
	    state = new LiveState();
        state.Start(gameObject, this);
	}
	
	// Update is called once per frame
	void Update () {
        state.Update();
	}

    public IEntityState Change<EntityState>() where EntityState : IEntityState, new()
    {
        state = new EntityState();
        state.Start(gameObject, this);
        return state;
    }

	public void HandleHit(double amount, Stunner stunner, Vector3 knockback)
	{
		state.HandleKnockback (knockback);
		HandleHit (amount, stunner);
	}

    public void HandleHit(double amount, Stunner stunner)
    {
        state.HandleHit(amount);
        if (stunner)
        {
            state.HandleStun(stunner);
        }
    }
    
    public IEntityState getState()
    {
    	return state;
    }

	public void DestroyObjectTimer(GameObject obj, float t)
	{
		DestroyObject (obj, t); 
	}
}
