﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SerializeAll]
public class HeroMoveSound : MonoBehaviour {

	//private List<AudioSource> walkWood;
	private AudioSource walkWood0;
	private AudioSource walkWood1;
	private Vector3 lastPosition;
	private int state = 0;

	// Use this for initialization
	void Start () {
		walkWood0 = gameObject.AddComponent<AudioSource>();
		walkWood1 = gameObject.AddComponent<AudioSource>();
		walkWood0.clip = Resources.Load("Audio/walk_wood_0") as AudioClip;
		walkWood1.clip = Resources.Load("Audio/walk_wood_1") as AudioClip;
		walkWood0.playOnAwake = false;
		walkWood1.playOnAwake = false;
		lastPosition = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.transform.position != lastPosition)
		{
			
			if (!walkWood1.isPlaying && state == 0) 
			{
				walkWood0.Play();
				state++;
			}
			else if (!walkWood0.isPlaying && state == 1) 
			{
				walkWood1.Play();
				state++;
			}
			lastPosition = gameObject.transform.position;
			state %= 2;
		}
	}
}
