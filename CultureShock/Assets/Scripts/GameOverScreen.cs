﻿using UnityEngine;
using System.Collections;

public class GameOverScreen : MonoBehaviour {
	private bool showLoads = false;
	private Vector2 scrollPosition;
	void Start()
	{
		GameObject.FindGameObjectWithTag ("PauseMenu").GetComponent<PauseMenu> ().enabled = false;
	}
	void OnGUI () 
	{

		//Time.timeScale = 0.0f;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "");
		GUIStyle box = "box";   
		GUILayout.BeginArea(new Rect( Screen.width/2 - 200,Screen.height/2 - 300, 400, 600), box);
		
		GUILayout.BeginVertical(); 
		GUILayout.FlexibleSpace();
		if(!showLoads)
		{
			if(GUILayout.Button ("Resume from Last Checkpoint"))
			{
				LevelSerializer.Resume();
			}
			if(GUILayout.Button("Load Game"))
			{
				showLoads = true;
			}
			else if(GUILayout.Button("Quit to Title Screen"))
			{
				Application.LoadLevel(0);
			}
			else if(GUILayout.Button("Quit to Desktop"))
			{
				Application.Quit();
			}
		}
		if(showLoads)
		{
			if(GUILayout.Button("Back"))
			{
				showLoads = false;
			}
			GUILayout.Space(60);
			scrollPosition = GUILayout.BeginScrollView(scrollPosition,GUILayout.Width(400), GUILayout.Height(200));
			foreach(var sg in LevelSerializer.SavedGames[LevelSerializer.PlayerName]) { 
				if(GUILayout.Button(sg.Caption)) 
				{ 
					LevelSerializer.LoadSavedLevel(sg.Data);
					Time.timeScale = 1;
				} 
			} 
			GUILayout.EndScrollView();
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}
