﻿using UnityEngine;
using System.Collections;

public static class ParticleEffectFactory {

    public static GameObject CreateBloodSpatter(GameObject parent)
    {
		var systemGameObj = CreateParticleSystem(parent, "BloodSprite", 0.6);
		var system = systemGameObj.GetComponent<SpriteParticleSystem>();
		system.force = 300;
		system.coolDownDuration = 0.15;
		return systemGameObj;
    }

	public static GameObject CreateParticleSystem(GameObject parent, string spriteTag, double sysLifetime) {
        var particleSystem = new GameObject();
		var spriteSys = particleSystem.AddComponent<SpriteParticleSystem>();
		spriteSys.sprite = GameObject.FindGameObjectWithTag(spriteTag).GetComponent<SpriteRenderer>().sprite;
		spriteSys.parent = parent;
		var lifetime = particleSystem.AddComponent<Lifetime>();
		lifetime.lifetime = sysLifetime;
        return particleSystem;
	}

	public static GameObject CreateAttackSystem(GameObject parent)
	{
		var systemGameObj = CreateParticleSystem(parent, "SwingSprite", parent.GetComponent<Lifetime>().lifetime);
		var system = systemGameObj.GetComponent<SpriteParticleSystem>();
		system.useGravity = false;
		system.coolDownDuration = 0.05;
		system.force = 1000;
		system.particleLifetime = 0.1;
		return systemGameObj;
	}
}