using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
		public GUISkin skin;
		public GUITexture pausedGUI;
		public string gameName = "Your Game";
		bool paused = false;
		bool showLoads = false;
		Vector2 scrollPosition;

		void Start ()
		{
				scrollPosition = Vector2.zero;
				if (pausedGUI)
						pausedGUI.enabled = false;
		}

		void Update ()
		{ 
				if (Input.GetKeyUp (KeyCode.P)) { 
						paused = !paused;
		
						if (paused == true) {
								Time.timeScale = 0.01f;
								if (pausedGUI)
										pausedGUI.enabled = true;
						} else {
								Time.timeScale = 1.0f;
								if (pausedGUI)
										pausedGUI.enabled = false;
						}
				}
		}

		void OnGUI ()
		{
			GUI.skin = skin;
				if (!paused) {
						GUILayout.BeginArea (new Rect (200, 10, 400, 50));
						GUILayout.BeginVertical ();
						GUILayout.BeginHorizontal ();
						GUILayout.FlexibleSpace ();
						GUILayout.Label ("Press P to Pause");
						GUILayout.FlexibleSpace ();
						GUILayout.EndHorizontal ();
						GUILayout.EndVertical ();
						GUILayout.EndArea ();
						return;
				}
	
				GUIStyle box = "box";   
				GUILayout.BeginArea (new Rect (Screen.width / 2 - 200, Screen.height / 2 - 300, 400, 600), box);
	
				GUILayout.BeginVertical (); 
				GUILayout.FlexibleSpace ();
				if (!showLoads) {
						/*if (GUILayout.Button ("Save Game")) {
								LevelSerializer.SaveGame (gameName);
						} else if (GUILayout.Button ("Load Game")) {
								showLoads = true;
						}*/
						if (GUILayout.Button ("Resume from Last Checkpoint")) 
						{
							LevelSerializer.Resume();
							Time.timeScale = 1;
						}
						/*else if (GUILayout.Button ("Save Checkpoint")) 
						{
							LevelSerializer.Checkpoint();
						}*/else if (GUILayout.Button ("Quit to Title Screen")) {
								Application.LoadLevel (0);
						} else if (GUILayout.Button ("Quit to Desktop")) {
								Application.Quit ();
						}
				}
				if (showLoads) {
						if (GUILayout.Button ("Back")) {
								showLoads = false;
						}
						GUILayout.Space (60);
						scrollPosition = GUILayout.BeginScrollView (scrollPosition, GUILayout.Width (400), GUILayout.Height (200));
						foreach (var sg in LevelSerializer.SavedGames[LevelSerializer.PlayerName]) { 
								if (GUILayout.Button (sg.Caption)) { 
										LevelSerializer.LoadSavedLevel (sg.Data);
										Time.timeScale = 1;
								} 
						} 
						GUILayout.EndScrollView ();
				}
				GUILayout.FlexibleSpace ();
				GUILayout.EndVertical ();
				GUILayout.EndArea ();
	
	
		}
}
