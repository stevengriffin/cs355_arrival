﻿using UnityEngine;
using System.Collections;

[SerializeAll]
public class Opening_GivePipe : MonoBehaviour {
	private TriggeredWaypointThenDialogue wdt; 
	public Vector3 disableAtWaypoint;
	enum AttackDemoState
	{
		GIVE,
		GIVE_WAIT,
		SWING1,
		SWING1_WAIT,
		SWING2,
		SWING2_WAIT,
		TEXT,
		AWAY
	}

	private AttackDemoState adt;
	private GameObject player;
	private float startTime;

	// Use this for initialization
	void Start () {
		if(LevelSerializer.IsDeserializing)
			return;
		adt = AttackDemoState.GIVE;
		wdt = gameObject.GetComponent<TriggeredWaypointThenDialogue> ();
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void Update()
	{
		if(adt == AttackDemoState.GIVE_WAIT)
		{
			if(Time.time > startTime + 1.0)
				adt = AttackDemoState.SWING1;
		}
		else if(adt == AttackDemoState.SWING1)
		{
			player.GetComponent<Attacker>().Attack();
			startTime = Time.time;
			adt = AttackDemoState.SWING1_WAIT;
		}
		else if(adt == AttackDemoState.SWING1_WAIT)
		{
			if(Time.time > startTime + 1.0)
				adt = AttackDemoState.SWING2;
		}
		else if(adt == AttackDemoState.SWING2)
		{
			player.GetComponent<Attacker>().Attack();
			startTime = Time.time;
			adt = AttackDemoState.SWING2_WAIT;
		}
		else if(adt == AttackDemoState.SWING2_WAIT)
		{
			if(Time.time > startTime + .5)
				adt = AttackDemoState.TEXT;
		}
		else if(adt == AttackDemoState.TEXT)
		{
			GUIStyle sty = new GUIStyle();
			sty.fontSize = 15;
			sty.normal.textColor = Color.white;
			sty.wordWrap = true;
			sty.alignment = TextAnchor.LowerCenter;
			
			TextFader tf = gameObject.AddComponent<TextFader> ();
			tf.t = player.transform;
			tf.duration = 6.0f;
			tf.text = "Use right-click to attack";
			tf.style = sty;
			tf.outColor = Color.black;
			tf.inColor = Color.gray;
			player.GetComponent<CharacterMotor>().canControl = true;
			gameObject.GetComponent<WaypointPath> ().enabled = true;
			
			adt = AttackDemoState.AWAY;
			LevelSerializer.Checkpoint ();
			

		}
		else if(adt == AttackDemoState.AWAY)
		{
			Vector3 dist = disableAtWaypoint - gameObject.transform.position;
			if(dist.magnitude < 3.0f)
			{
				Destroy(gameObject.GetComponent<WaypointPath>());
				gameObject.SetActive(false);
				enabled = false;
			}
		}
	}

	
	// Update is called once per frame
	void OnGUI() {
		if(adt == AttackDemoState.GIVE && wdt && wdt.GetDialogueState() == DialogueAboveHead.DialogueState.DONE)
		{
			player.GetComponent<CharacterMotor>().canControl = false;
			GUIStyle sty = new GUIStyle();
			sty.fontSize = 12;
			sty.normal.textColor = Color.white;
			sty.wordWrap = true;
			sty.alignment = TextAnchor.LowerCenter;
			
			TextFader tf = gameObject.AddComponent<TextFader> ();
			tf.t = player.transform;
			tf.duration = 3.0f;
			tf.text = "Got a Pipe";
			tf.style = sty;
			tf.outColor = Color.black;
			tf.inColor = Color.gray;
			
			Inventory inv = GameObject.FindGameObjectWithTag("Inventory").GetComponent<Inventory>();
			Item pipe = (Item)Content.fullCategory["Pipe"];
			inv.get(pipe);
			pipe.Use(player,null);
			player.GetComponent<Attacker>().enabled = true;
			startTime = Time.time;
			adt = AttackDemoState.GIVE_WAIT;
		}
	}
}
