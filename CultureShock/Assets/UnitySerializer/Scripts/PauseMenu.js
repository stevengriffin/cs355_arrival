import System.Collections.Generic;

var paused : boolean = false; 
var showLoads : boolean = false;
var pausedGUI : GUITexture;  
var gameName : String = "Your Game";
var scrollPosition : Vector2;
var myList = new List.<Transform>();

function Start()
{
	scrollPosition = Vector2.zero;
	if(pausedGUI)
		pausedGUI.enabled = false;
}

function Update () 
{ 
    if(Input.GetKeyUp(KeyCode.P))
    { 
       paused = !paused;

	    if(paused == true){
	        Time.timeScale = 0.0;
	        if(pausedGUI) pausedGUI.enabled = true;
	    } else {
	        Time.timeScale = 1.0;
	        if(pausedGUI) pausedGUI.enabled = false;
	    }
   }
}

function OnGUI() {
	if(!paused)
	{
	   GUILayout.BeginArea(Rect(200,10,400,20));
	   GUILayout.BeginVertical();
	   GUILayout.BeginHorizontal();
	   GUILayout.FlexibleSpace();
	   GUILayout.Label("Press P to Pause");
	   GUILayout.FlexibleSpace();
	   GUILayout.EndHorizontal();
	   GUILayout.EndVertical();
	   GUILayout.EndArea();
	   return;
	}
	   
	var box : GUIStyle = "box";   
    GUILayout.BeginArea(Rect( Screen.width/2 - 200,Screen.height/2 - 300, 400, 600), box);

    GUILayout.BeginVertical(); 
    GUILayout.FlexibleSpace();
    if(!showLoads)
    {
    	if(GUILayout.Button("Save Game"))
    	{
    		LevelSerializer.SaveGame(gameName);
    	}
    	else if(GUILayout.Button("Load Game"))
    	{
    		showLoads = true;
    	}
    	else if(GUILayout.Button("Quit to Title Screen"))
    	{
    		Application.LoadLevel(0);
    	}
    	else if(GUILayout.Button("Quit to Desktop"))
    	{
    		Application.Quit();
    	}
    }
    if(showLoads)
    {
       	if(GUILayout.Button("Back"))
    	{
    		showLoads = false;
    	}
    	GUILayout.Space(60);
    	scrollPosition = GUILayout.BeginScrollView(scrollPosition,GUILayout.Width(400), GUILayout.Height(200));
    	for(var sg in LevelSerializer.SavedGames[LevelSerializer.PlayerName]) { 
    	   if(GUILayout.Button(sg.Caption)) { 
    	     LevelSerializer.LoadSavedLevel(sg.Data);
    	     Time.timeScale = 1;
    	   } 
    	} 
    	GUILayout.EndScrollView();
    }
    GUILayout.FlexibleSpace();
    GUILayout.EndVertical();
    GUILayout.EndArea();


}
