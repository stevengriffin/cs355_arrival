"""
Arrival Studios
Runs simulations for the CultureShock board game.
"""
import argparse
import random
import os
import math
from multiprocessing import Pool
from itertools import product
from collections import defaultdict
from cPickle import PicklingError


class Args(object):

    """Container for command-line arguments."""

    NUM_TRIALS = 10000
    DEBUG = False
    PRODUCT = False


class Character(object):

    """Represents a player in the game with some skills."""

    def __init__(self, food, clothing, lodging, employment):
        """Set skills from necessity tokens."""
        self.movement = food + 2 * lodging
        self.defense = clothing
        self.upkeep = food + 2 * lodging - employment

    def __str__(self):
        return "M: " + str(self.movement) + " D: " + str(self.defense) + \
            " U: " + str(self.upkeep)


def makeCharacters():
    """Makes a list of character necessity possibilities."""
    if Args.PRODUCT:
        # Generates all possible skill combinations.
        # This defines what necessity levels are possible.
        necs = (0, 1, 2)
        return [Character(*s) for s in product(necs, necs, necs, necs)]

    else:
        # Since only clothing is relevant for these combat sims
        # we set the rest to 0.
        return [Character(0, c, 0, 0) for c in xrange(5)]

#
# Simulation functions. Matches a single character
# against another or a challenge. Return 0 if the first
# character loses, 0.5 for a tie, and 1 if the first character wins.
#


def chooseWinner(int1, int2):
    if int1 > int2:
        return 1
    if int1 < int2:
        return 0
    return 0.5


def mug(tupleOfCharacters):
    """
    Has the second character try to mug the first.
    We do it in that order because only the defense
    matters.
    """
    # Roll a D8
    attackRoll = random.randint(1, 8)
    # Defending player rolls D6 + defense.
    defendRoll = random.randint(1, 6) + tupleOfCharacters[0].defense
    # Winner is higher die roll.
    return chooseWinner(defendRoll, attackRoll)


def encounter(tupleChar):
    """
    Has a character fight an encounter.
    """
    # Encounter difficulty is on a card and between 2 and 8
    encounterDifficulty = random.randint(2, 8)
    # Defending player rolls D8 + defense.
    defendRoll = random.randint(1, 8) + tupleChar[0].defense
    # Winner is higher number.
    return chooseWinner(defendRoll, encounterDifficulty)

#
# End simulation functions.
#


def doTrials(tupleOfCharacters, trialFunc):
    """
    Runs a trial function NUM_TRIALS times on each character matchup
    and sums the resulting tuples.
    """
    sums = sum(map(trialFunc,
                   (tupleOfCharacters for i in xrange(Args.NUM_TRIALS))))
    return (str(tupleOfCharacters[0]),
            sums / Args.NUM_TRIALS)


def processResults(results):
    """Manipulates results list into form more suitable for printing."""
    # Merge into unique characters. Dict keys default to 0.
    resultDict = defaultdict(int)
    counterDict = defaultdict(int)
    for res in results:
        resultDict[res[0]] += res[1]
        counterDict[res[0]] += 1
    for key in resultDict:
        resultDict[key] /= counterDict[key]
    uniqueResults = [(key, resultDict[key]) for key in resultDict]
    # Sort results by percentage won
    return sorted(uniqueResults, key=lambda r: r[1])

if os.name == 'posix':
    # Assumes color terminal. Won't work in Windows command prompt.
    END_COLOR = "\033[0m"
    RED = "\033[91m"
    GREEN = "\033[92m"
else:
    END_COLOR = ""
    RED = ""
    GREEN = ""


def printTrials(results, name):
    print(name + " Results:")
    for res in results:
        if res[1] >= 0.5:
            color = GREEN
        else:
            color = RED
        string = '{} === ' + color + '{:.2%}' + END_COLOR
        print(string.format(res[0], res[1]))


def main(**kwargs):
    Args.NUM_TRIALS = kwargs['num_trials']
    Args.DEBUG = kwargs['debug']
    Args.PRODUCT = kwargs['product']
    # Seed random with current system time.
    random.seed()
    print("Running " + str(Args.NUM_TRIALS) + " trials for each.")
    print("99% confident that true means are within +/- " +
          "{:.2%} of sample means.".format(
              0.5 * 2.576 / math.sqrt(Args.NUM_TRIALS)))

    if not Args.DEBUG:
        # Process pool. Defaults to number of CPUs.
        pool = Pool()

    def performAndPrintTrials(trialFunc, trialChars, trialName):
        """
        Launch processes to execute a simulation on each
        character match and print the results.
        """
        if not Args.DEBUG:
            # multi-processing version
            # Use this one for better performance.
            # It confuses stack traces though so use single-threaded for
            # debugging.
            try:
                results = pool.map(lambda c: doTrials(c, trialFunc),
                                   trialChars)
            except PicklingError:
                print("""It looks like you tried to run this in
                    multiprocessing mode using CPython. Either
                    use single process mode (python sim.py -d) or
                    use the PyPy JIT compiler.""")
                return False
        else:
            # single-threaded version
            results = map(lambda c: doTrials(c, trialFunc), trialChars)
        # Print results
        printTrials(processResults(results), trialName)
        return True

    # List of character skill combinations
    CHARACTERS = makeCharacters()
    # Dictionary of simulation functions and character input.
    TRIAL_FUNCS = {"Mugging": (mug, product(CHARACTERS, CHARACTERS)),
                   "Encounter": (encounter, [(c,) for c in CHARACTERS]),
                   }
    # Perform trial using each defined trial function.
    for name in TRIAL_FUNCS:
        if not performAndPrintTrials(TRIAL_FUNCS[name][0],
                                     TRIAL_FUNCS[name][1],
                                     name):
            return

if __name__ == '__main__':
    # Parses command line arguments and then calls main.
    parser = argparse.ArgumentParser(
        description='Run sim of CultureShock board game',
        version='1.0')
    parser.add_argument('-n', '--num-trials', type=int,
                        default=Args.NUM_TRIALS,
                        help='Number of trials to run, default ' +
                        str(Args.NUM_TRIALS))
    parser.add_argument('-p', '--product',
                        help='use cartesian product of characters',
                        action="store_true")
    parser.add_argument('-d', '--debug',
                        help='run in debug mode', action="store_true")
    ARGS_DICT = parser.parse_args()
    main(**vars(ARGS_DICT))
